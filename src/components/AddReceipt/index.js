import React,{useState} from 'react';
import {Button,Card,Col,Row,Form,InputGroup} from "@themesberg/react-bootstrap";
import {Formik} from 'formik';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt ,faPlus,faBackward,faPenSquare,faTrash} from '@fortawesome/free-solid-svg-icons';


const CreateSchool = () => {
  const initialValue={
    prefix:"",
    suffix:"",
    startNumber:"",
    endNumber:"",
    school:"",
    academicYear:"",
    receiptType:"",
  }
  const handleSubmitForm=(values)=>{
    console.log(values)

  }
  return (
    <Card border="light" className="bg-white shadow-sm mb-4">
    <h4>Fee Receipt</h4>
      <Card.Body>
       <Formik
       initialValues={initialValue}
       onSubmit={handleSubmitForm}
      //  validationSchema={validationStudentRegForm}
       >
       {({
        errors,
        values,
        touched,
        handleChange,
        handleSubmit,
        resetForm,
        isSubmiting,
        handleBlur,
       })=>{
        return(
          <Form onSubmit={handleSubmit} >
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="prefix">
                <Form.Label>First name *</Form.Label>
                   <Form.Control
                   name="prefix"
                    type='text'
                    placeholder='enter first name'
                    value={values.prefix}
                    onChange={handleChange}
                   />
                 <div style={{color:"red"}} >
                  {errors.fullName && touched.fullName && errors.fullName}
                 </div>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="suffix">
                <Form.Label>Last name *</Form.Label>
                <Form.Control 
                name="suffix"
                value={values.suffix}
                onChange={handleChange}
                onBlur={handleBlur}
                errors={errors.suffix}
                required type="text" 
                placeholder="enter last name" 

                />
              </Form.Group>
            </Col>
          </Row>
          <Row className="align-items-center mt-2">
            <Col md={6} className="mb-3">
              <Form.Group id="startNumber">
                <Form.Label>Start Number *</Form.Label>
                 <Form.Control
                  type='text'
                  placeholder='enter start number'
                  name="startNumber"
                  value={values.startNumber}
                  onChange={handleChange}
                  onBlur={handleBlur}
                 />
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="endnumber">
                <Form.Label>End Number *</Form.Label>
               <Form.Control
               name='endNumber'
                type="text"
                placeholder="enter end number"
                value={values.endNumber}
                onChange={handleChange}
                onBlur={handleBlur}
               />
              </Form.Group>
            </Col>
          </Row>
          <Row className='mt-2' >
            <Col md={6} className="mb-3">
              <Form.Group id="email">
                <Form.Label>Select school *</Form.Label>
                  <Form.Select
                  name='school'
                  value={values.school}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  >
                   <option>Select school</option>
                   <option>Mount Carmel school</option>
                   <option>st johns school</option>
                   <option>Vigour school</option>
                   <option>public school</option>
                  </Form.Select>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="phone">
                <Form.Label>Academic year *</Form.Label>
               <Form.Select
                 name="academicYear"
                 value={values.academicYear}
                 onChange={handleChange}
                 lonBlur={handleBlur}
               >
               <option>please select</option>
                <option>2020-2021</option>
                <option>2021-2022</option>
                <option>2022-2023</option>
                <option>2023-2024</option>
               </Form.Select>
              </Form.Group>
            </Col>
          </Row>
          <Row className='mt-2' >
            <Col className='mb-3' md={6} >
            <Form.Group>
              <Form.Label>Select receipt type</Form.Label>
              <Form.Select
              name='receiptType'
              value={values.receiptType}
              onChange={handleChange}
              >
                <option>please select</option>
                <option>Automatic</option>
                <option>Manually</option>
              </Form.Select>
            </Form.Group>

            </Col>
          </Row>
          <div className="mt-3">
            <Button variant="primary" type="submit" onClick={handleSubmit} >Submit</Button>
          </div>
        </Form>
        )
       }}
       </Formik>
      </Card.Body>
    </Card>
  )
}

export default CreateSchool