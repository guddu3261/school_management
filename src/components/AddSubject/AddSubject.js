import React from 'react'
import { Card, Col, Form, Row } from '@themesberg/react-bootstrap'
const AddSubject = () => {
    
  return (
    <Card border="light" className="bg-white shadow-sm mb-0">
    <Card.Body>
    <h6 className="text-center" mb-4>Add Subject(for College)</h6>
        <Form>
            <Row className="mt-3">
            <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Subject name</Form.Label>
                          <Form.Control
                            type="text"
                            placeholder='enter subject name'
                          />
                    </Form.Group>
                </Col>
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label> subject code</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="enter subject code"
                        />
                    </Form.Group>
                </Col>
            </Row>
        </Form>
    </Card.Body>
    <Card.Body>
    <h6 className="text-center" mt-4>Add Subject(for School)</h6>
    <Form>
            <Row className="mt-3">
            <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Class</Form.Label>
                           <Form.Select> class
                           <option>select class</option>
                           <option>class 1</option>
                           <option>class 2</option>
                           <option>class 3</option>
                           <option>class 4</option>
                           </Form.Select>
                          
                    </Form.Group>
                </Col>
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label> subject </Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="enter subject"
                        />
                    </Form.Group>
                </Col>
            </Row>
        </Form>
    </Card.Body>
   </Card>
  )
}

export default AddSubject