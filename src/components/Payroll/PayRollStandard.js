import React,{useState,useEffect} from 'react';
import formik from 'formik'
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';

const PayRollStandard = () => {
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
    <h4 className='text-center' >Enter Payroll Details</h4>
        <Card.Body>
        <Form>
        <h5 className='my-2' >Provident Fund:</h5>
            <Row>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>PF Account Prefix:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='PF'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>PF Max Amount:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='1800'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>FPF %:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='8.75'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>EPF %:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='3.25'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>FPF %:</Form.Label>
                        <Form.Control
                            type='password'
                            placeholder='8.75'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>FPF %:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='8.75'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>Acc No. 21 %:</Form.Label>
                       <Form.Control
                           type='text'
                           placeholder='0.5'
                       />
                   </Form.Group>
               </Col>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>A/C21 Min Amount:</Form.Label>
                       <Form.Control
                           type='password'
                           placeholder='100'
                       />
                   </Form.Group>
               </Col>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>Acc No. 21 %:</Form.Label>
                       <Form.Control
                           type='text'
                           placeholder='100'
                       />
                   </Form.Group>
               </Col>
           </Row>
           <Row>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>Acc No. 22 %:</Form.Label>
                       <Form.Control
                           type='text'
                           placeholder='0.95'
                       />
                   </Form.Group>
               </Col>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>A/C22 Min Amount:</Form.Label>
                       <Form.Control
                           type='password'
                           placeholder='100'
                       />
                   </Form.Group>
               </Col>
               <Col md={4} className='mb-4'>
                   <Form.Group>
                       <Form.Label>Acc No. 22 %:</Form.Label>
                       <Form.Control
                           type='text'
                           placeholder='100'
                       />
                   </Form.Group>
               </Col>
           </Row>
           <h5 className='my2'>Employee</h5>
           <Row>
               <Col md={4} className='mb-4'>
                 <Form.Group>
                    <Form.Label>Retirement Years:</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='58'
                    />
                 </Form.Group>
               </Col>

            <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>Salary  From Day:</Form.Label>
                    <Form.Select>
                        <option>please select</option>
                    </Form.Select>
                </Form.Group>
            </Col>
            <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>Salary  To Day:</Form.Label>
                    <Form.Select>
                        <option>please select</option>
                    </Form.Select>
                </Form.Group>
            </Col>
           </Row>
           <h5 className='my-3' >ESI</h5>
           <Row>
            <Col md={6} className='mb-3'>
                <Form.Group>
                    <Form.Label>ESI Account Prefix:</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='ESI'
                    />
                </Form.Group>
            </Col>
            <Col md={6} className='mb-3'>
                <Form.Group>
                    <Form.Label>ESI Max (%):</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='1.75'
                    />
                </Form.Group>
            </Col>
           </Row>
          <Row>
          <Col md={6} className='mb-3'>
                <Form.Group>
                    <Form.Label>ESI Max Amount:</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='21000'
                    />
                </Form.Group>
            </Col>
          <Col md={6} className='mb-3'>
                <Form.Group>
                    <Form.Label>Employer Contribution (%):</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='4.25'
                    />
                </Form.Group>
            </Col>
          </Row>
          {/* <h5 className="my-4">Arrear Salary Report</h5> */}
          <Row  className='mt-4' >
           <Col md={4} className='mb-3'>
            <Form.Group>
                <h6>Arrear Salary Report</h6>
                  <fieldset>
                  <Form.Check
                     defaultChecked
                      type="radio"
                      defaultValue="option1"
                      label="Separate Arrear Salary Report"
                      name="exampleRadios"
                       id="radio1"
                       htmlFor="radio1"
                     />
                      <Form.Check
                        type="radio"
                        defaultValue="option2"
                        label="Only Arrear Head"
                         name="exampleRadios"
                        id="radio2"
                        htmlFor="radio2"
                     />
                  </fieldset>
             </Form.Group>
           </Col>
           <Col md={4} className='mb-4'>
            <Form.Group>
                <h6>Approval Process</h6>
                <Form.Check label="Salary Advance" id="checkbox1" htmlFor="checkbox1" />
                <Form.Check  label="Loan" id="checkbox2" htmlFor="checkbox2" />
                <Form.Check  label="Leave" id="checkbox2" htmlFor="checkbox3" />
                <Form.Check  label="Increment" id="checkbox2" htmlFor="checkbox4" />
                <Form.Check  label="Probation checkbox" id="checkbox2" htmlFor="checkbox5" />
            </Form.Group>
           </Col>
           <Col md={4} className='mb-3'>
            <Form.Group>
                <h6>Payment Method</h6>
                  <fieldset>
                  <Form.Check
                     defaultChecked
                      type="radio"
                      defaultValue="option1"
                      label="Method 1"
                      name="paymentMethod"
                      id="radio1"
                       htmlFor="radio1"
                     />
                      <Form.Check
                       type="radio"
                       defaultValue="option2"
                       label="Method 2"
                       name="paymentMethod"
                       id="radio2"
                       htmlFor="radio2"
                     />
                  </fieldset>
             </Form.Group>
           </Col>
          </Row>
          <Button type='submit'>submit</Button>
        </Form>
        </Card.Body>
    </Card>
  )
}

export default PayRollStandard;