import React, { useState } from "react";
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt ,faPlus,faBackward,faPenSquare,faTrash} from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Card, Form, Button, InputGroup } from '@themesberg/react-bootstrap';
import ArrowBackIosSharpIcon from '@mui/icons-material/ArrowBackIosSharp';import {
  Nav,
  Image,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  
} from "@themesberg/react-bootstrap";
import { Formik } from 'formik';
import * as Yup from 'yup';
export const GeneralInfoForm = () => {
  const[showTable,setShowTable]=useState(true);
  const[showAddForm,setShowAddForm]=useState(false);
  const [birthday, setBirthday] = useState("");
  const[studentArray,setStudentArray]=useState([
    {name:"Rahul",RollNo:23,regNo:"BH245Ty",class:"8th",age:19,batch:"C",parentName:"Rahul Raj"},
    {name:"Smith",RollNo:13,regNo:"BH235Ty",class:"7th",age:18,batch:"A",parentName:"Kisan Lal"},
    {name:"Ramesh",RollNo:23,regNo:"BH234Ty",class:"10th",age:23,batch:"B",parentName:"Kanhaiya Lal"},
    {name:"Ramesh",RollNo:39,regNo:"BH234Ty",class:"10th",age:19,batch:"A",parentName:"Kanhaiya Lal"},
  ]);
  const initialValue={
    fullName:"",
    parentName:"",
    dob:"",
    gender:"",
    email:"",
    phone:"",
    address:"",
    city:"",
    pinCode:"",
  }
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const validationStudentRegForm=Yup.object().shape({
    fullName:Yup.string().required('Required *'),
    email:Yup.string().email('enter a valid email').required("Required *"),
    phone:Yup.string().matches(phoneRegExp, 'Phone number is not valid').required('Required *'),
    address:Yup.string().required('Required *'),
    pinCode:Yup.string().required('Required *'),

  })
const handleAddForm=()=>{
  setShowAddForm(true);
  setShowTable(false);
}
const handleSubmitForm=(values)=>{
  console.log(values);
}
  return (
   <>
       {showTable == true && (
        <>
        <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            {/* <h4>Class</h4> */}
          </div>
          <Dropdown className="btn-toolbar" onClick={() => handleAddForm()}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faPlus} cNewlassName="me-2" />
              Add Student
            </Dropdown.Toggle>
          </Dropdown>
        </div>
        <Card className="shadow-sm mb-4" border="light">
          <Card.Body className="pb-0">
            <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Roll No.</th>
                  <th className="border-0">Register No</th>
                  <th className="border-0">Student Name</th>
                  <th className="border-0">Class</th>
                  <th className="border-0">Batch</th>
                  <th className="border-0">Age</th>
                  <th className="border-0">Parent Name</th>
                  <th className="border-0">Action</th>
                  {/* <th className='border-0'>Slno.</th> */}
                </tr>
              </thead>
              <tbody>
                {studentArray.map((item,i) => (
                  <tr key ={i}>
                    <td>{i+1}</td>
                    <td>{item.name}</td>
                    <td>{item.RollNo}</td>
                    <td>{item.regNo}</td>
                    <td>{item.class}</td>
                    <td>{item.age}</td>
                    <td>{item.batch}</td>
                    <td>{item.parentName}</td>
                    <td>
                  <Row>
                    <Col md={2}>
                    <FontAwesomeIcon 
                    className='icon_title'
                    icon={faPenSquare}
                    onClick={()=>{
                      setShowAddForm(true);
                      setShowTable(false);
                    }}
                    />
                    </Col>
                    <Col>
                   
                     <FontAwesomeIcon icon={faTrash}
                    // onClick={()=>{
                    //   setShowAddForm(true);
                    //   setShowTable(false);
                    // }}
                     />
                    </Col>
                  </Row>
                    </td>
                    
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
        </>
      )}
     {showAddForm==true &&(
      <>
      <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            {/* <h4>Class</h4> */}
          </div>
          
          <Dropdown className="btn-toolbar" onClick={() =>{
            setShowAddForm(false);
            setShowTable(true)
          }}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faBackward} cNewlassName="me-2" />
            </Dropdown.Toggle>
          </Dropdown>
        </div>
      {/* <Row  xs={12} xl={16} >
        <Col xs={12} xl={8}>
        <h5 className="mb-4 mt-4">Student Registration</h5>
        </Col>
        <Col xs={12} xl={2} className='float-right' >
         <div style={{color:"black" }}  >
         <ArrowBackIosSharpIcon
            onClick={()=>{
              setShowAddForm(false)
              setShowTable(true)
              // resetForm();
            }}
          />
         </div>
        </Col>
      </Row> */}
      
      <Card border="light" className="bg-white shadow-sm mb-4">
      <Card.Body>
       <Formik
       initialValues={initialValue}
       onSubmit={handleSubmitForm}
       validationSchema={validationStudentRegForm}
       >
       {({
        errors,
        values,
        touched,
        handleChange,
        handleSubmit,
        resetForm,
        isSubmiting,
        handleBlur,
       })=>{
        return(
          <Form onSubmit={handleSubmit} >
          <h5 className="my-4">Personal Details</h5>
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="firstName">
                <Form.Label>Full Name</Form.Label>
                <Form.Control 
                name="fullName"
                value={values.fullName}
                onChange={handleChange}
                required type="text"
                onBlur={handleBlur}
                 placeholder="Enter your first name" 
                 errors={errors.fullName}
                 />
                 <div style={{color:"red"}} >
                  {errors.fullName && touched.fullName && errors.fullName}
                 </div>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="lastName">
                <Form.Label>Parent Name</Form.Label>
                <Form.Control 
                name="parentName"
                value={values.parentName}
                onChange={handleChange}
                onBlur={handleBlur}
                errors={errors.parentName}
                required type="text" 
                placeholder="Enter your parent name" 

                />
              </Form.Group>
            </Col>
          </Row>
          <Row className="align-items-center">
            <Col md={6} className="mb-3">
              <Form.Group id="birthday">
                <Form.Label>Date of Birth</Form.Label>
                <Datetime
                  timeFormat={false}
                  onChange={setBirthday}
                  renderInput={(props, openCalendar) => (
                    <InputGroup>
                      <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                      <Form.Control
                        required
                        type="text"
                        name="dob"
                        // value={values.dob}
                        // onChange={handleChange}
                        value={birthday ? moment(birthday).format("MM/DD/YYYY") : ""}
                        placeholder="mm/dd/yyyy"
                        onFocus={openCalendar}
                        onChange={() => { }} />
                    </InputGroup>
                  )} />
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="gender">
                <Form.Label>Gender</Form.Label>
                <Form.Select defaultValue="0">
                  <option value="0">Gender</option>
                  <option value="1">Female</option>
                  <option value="2">Male</option>
                </Form.Select>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="emal">
                <Form.Label>Email</Form.Label>
                <Form.Control
                name="email"
                value={values.email}
                onChange={handleChange}
                errors={errors.email}
                 required type="email" 
                 placeholder="name@company.com" />
                 <div style={{color:"red"}} >
                 {errors.email && touched.email&& errors.email}
                 </div>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="phone">
                <Form.Label>Phone</Form.Label>
                <Form.Control
                name='phone'
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
                errors={errors.phone}
                 required type="number"
                  placeholder="+91-345 678 910" />
                  <div style={{color:'red'}}>
                    {errors.phone}
                  </div>
              </Form.Group>
            </Col>
          </Row>

          <h5 className="my-4">Address</h5>
          <Row>
            <Col sm={9} className="mb-3">
              <Form.Group id="address">
                <Form.Label>Address</Form.Label>
                <Form.Control
                name='address'
                value={values.address}
                onChange={handleChange}
                onBlur={handleBlur}
                 required type="text" 
                 placeholder="Enter your home address"
                 error={errors.address}
                  />
                  <div style={{color:'red'}} >
                    {errors.address}
                  </div>
              </Form.Group>
            </Col>
            {/* <Col sm={3} className="mb-3">
            <Form.Group id="city">
                <Form.Label>City</Form.Label>
                <Form.Control required type="text" placeholder="City" />
              </Form.Group>
            </Col> */}
          </Row>
          <Row>
            <Col sm={4} className="mb-3">
              <Form.Group id="city">
                <Form.Label>City</Form.Label>
                <Form.Control
                name='city'
                value={values.city}
                onChange={handleChange}
                onBlur={handleBlur}
                 required type="text"
                  placeholder="City" />
              </Form.Group>
              
            </Col>
            <Col sm={4} className="mb-3">
              <Form.Group className="mb-2">
                <Form.Label>Select state</Form.Label>
                <Form.Select id="state" defaultValue="0">
                  <option value="0">State</option>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col sm={4}>
              <Form.Group id="zip">
                <Form.Label>Pin Code</Form.Label>
                <Form.Control 
                name='pinCode'
                value={values.pinCode}
                onChange={handleChange}
                onBlur={handleBlur}
                required type="tel"
                 placeholder="pinCode" />
              </Form.Group>
              <div style={{color:'red'}}>
                {errors.pinCode && touched.pinCode && errors.pinCode}
              </div>
            </Col>
          </Row>
          <div className="mt-3">
            <Button variant="primary" type="submit" onClick={handleSubmit} >Save</Button>
          </div>
        </Form>
        )
       }}
       </Formik>
      </Card.Body>
    </Card>
      </>
    )}
   </>
  );
};
