import React,{useState} from 'react';
import {Button,Card,Col,Row,Form,InputGroup} from "@themesberg/react-bootstrap";
import {Formik} from 'formik';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt ,faPlus,faBackward,faPenSquare,faTrash} from '@fortawesome/free-solid-svg-icons';


const CreateCollege = () => {
  const[birthday,setBirthday]=useState("");
  const initialValue={
    collegeName:"",
    university:"",
    reg_no:"",
    website:"",
    email:"",
    phone:"",
    address:"",
    city:"",
    pinCode:"",
  }
  return (
    <Card border="light" className="bg-white shadow-sm mb-4">
    <h4>College Registration</h4>
      <Card.Body>
       <Formik
       initialValues={initialValue}
      //  onSubmit={handleSubmitForm}
      //  validationSchema={validationStudentRegForm}
       >
       {({
        errors,
        values,
        touched,
        handleChange,
        handleSubmit,
        resetForm,
        isSubmiting,
        handleBlur,
       })=>{
        return(
          <Form onSubmit={handleSubmit} >
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="firstName">
                <Form.Label>Select university *</Form.Label>
                  <Form.Select>
                    <option>Select</option>
                    <option>University1</option>
                    <option>University2</option>
                    <option>University3</option>
                    <option>University4</option>
                    <option>University5</option>
                    <option>University6</option>
                  </Form.Select>
                 <div style={{color:"red"}} >
                  {errors.fullName && touched.fullName && errors.fullName}
                 </div>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="lastName">
                <Form.Label>College Name *</Form.Label>
                <Form.Control 
                name="parentName"
                value={values.parentName}
                onChange={handleChange}
                onBlur={handleBlur}
                errors={errors.parentName}
                required type="text" 
                placeholder="Enter school name" 

                />
              </Form.Group>
            </Col>
          </Row>
          <Row className="align-items-center">
            <Col md={6} className="mb-3">
              <Form.Group id="birthday">
                <Form.Label>College Registration no *</Form.Label>
                 <Form.Control
                  type='text'
                  placeholder='enter registration no.'
                 />
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="gender">
                <Form.Label>College Website *</Form.Label>
               <Form.Control
                type="text"
                placeholder="www.college.com"
               />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col md={6} className="mb-3">
              <Form.Group id="emal">
                <Form.Label>Email *</Form.Label>
                <Form.Control
                name="email"
                value={values.email}
                onChange={handleChange}
                errors={errors.email}
                 required type="email" 
                 placeholder="name@company.com" />
                 <div style={{color:"red"}} >
                 {errors.email && touched.email&& errors.email}
                 </div>
              </Form.Group>
            </Col>
            <Col md={6} className="mb-3">
              <Form.Group id="phone">
                <Form.Label>Phone *</Form.Label>
                <Form.Control
                name='phone'
                value={values.phone}
                onChange={handleChange}
                onBlur={handleBlur}
                errors={errors.phone}
                 required type="number"
                  placeholder="+91-345 678 910" />
                  <div style={{color:'red'}}>
                    {errors.phone}
                  </div>
              </Form.Group>
            </Col>
          </Row>

          <h5 className="my-4">Address</h5>
          <Row>
            <Col sm={9} className="mb-3">
              <Form.Group id="address">
                <Form.Label>Address</Form.Label>
                <Form.Control
                name='address'
                value={values.address}
                onChange={handleChange}
                onBlur={handleBlur}
                 required type="text" 
                 placeholder="Enter college address"
                 error={errors.address}
                  />
                  <div style={{color:'red'}} >
                    {errors.address}
                  </div>
              </Form.Group>
            </Col>
            {/* <Col sm={3} className="mb-3">
            <Form.Group id="city">
                <Form.Label>City</Form.Label>
                <Form.Control required type="text" placeholder="City" />
              </Form.Group>
            </Col> */}
          </Row>
          <Row>
            <Col sm={4} className="mb-3">
              <Form.Group id="city">
                <Form.Label>City</Form.Label>
                <Form.Control
                name='city'
                value={values.city}
                onChange={handleChange}
                onBlur={handleBlur}
                 required type="text"
                  placeholder="City" />
              </Form.Group>
              
            </Col>
            <Col sm={4} className="mb-3">
              <Form.Group className="mb-2">
                <Form.Label>Select state</Form.Label>
                <Form.Select id="state" defaultValue="0">
                  <option value="0">State</option>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col sm={4}>
              <Form.Group id="zip">
                <Form.Label>Pin Code</Form.Label>
                <Form.Control 
                name='pinCode'
                value={values.pinCode}
                onChange={handleChange}
                onBlur={handleBlur}
                required type="tel"
                 placeholder="pinCode" />
              </Form.Group>
              <div style={{color:'red'}}>
                {errors.pinCode && touched.pinCode && errors.pinCode}
              </div>
            </Col>
          </Row>
          <div className="mt-3">
            <Button variant="primary" type="submit" onClick={handleSubmit} >Save</Button>
          </div>
        </Form>
        )
       }}
       </Formik>
      </Card.Body>
    </Card>
  )
}

export default CreateCollege;