import React from 'react';
import{Row,Col,Button,Form,Card,Table} from '@themesberg/react-bootstrap';

const AddImages = () => {
  return (
   <Card border='light' className='bg-white shadow-sm mb-3'>
    <Card.Body>
       <h4 className='text-center'>Enter Images Details</h4>
        <Form>
            <Row className='mt-4' >
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Image Short Name:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='enter image short name'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Image Short Description:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='description'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Add image</Form.Label>
                        <Form.Control
                            type='file'
                            placeholder='image should be hd quality'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Button type='submit'>Save</Button>
        </Form>
    </Card.Body>
   </Card> 
  )
}

export default AddImages