import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarAlt } from '@fortawesome/free-solid-svg-icons';

const LeaveApply = () => {
    const[leaveStartDate,setLeavStartDate]=useState("");
    const[leaveEndDate,setLeaveEndDate]=useState("");
  return (
    <Card className='bg-white shadow-sm mb-4' >
     <Card.Body>
       <Row >
        <h4 className='text-center' >Leave Apply</h4>
       </Row>
        <Form>
            <Row className='mt-3' >
            <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>Leave type</Form.Label>
                     <Form.Select>
                        <option>please select</option>
                        <option>Sick Leave</option>
                        <option>Casual leave</option>
                        <option>Formal Leave</option>
                     </Form.Select>
                </Form.Group>
            </Col>
             <Col md={4} className='mb-4'>
                <Form.Group>
                   <Form.Label>From date</Form.Label>
                   <Datetime
                           timeFormat={false}
                           onChange={setLeavStartDate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={leaveStartDate ? moment(leaveStartDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                    </InputGroup>
                  )} />
                </Form.Group>
             </Col>
             <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>to date</Form.Label>
                    <Datetime
                           timeFormat={false}
                           onChange={setLeaveEndDate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={leaveEndDate ? moment(leaveEndDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                    </InputGroup>
                  )} />
                </Form.Group>
             </Col>
            </Row>
            <Button type='submit'>submit</Button>
        </Form>
     </Card.Body>
    </Card>
  )
}

export default LeaveApply