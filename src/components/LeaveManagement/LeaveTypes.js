import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';

const LeaveTypes = () => {
    const[showAddForm,setShowAddForm]=useState(true);
    const[showList,setShowList]=useState(false);
    const handleLeaveTypes=()=>{
        setShowList(true);
    }
  return (
    <Card className='bg-white shadow-sm mb-4' border='light'>
        <Card.Body>
            <Row>
            <h5  className="text-center">Leave Type Details</h5>
            </Row>
            <Form>
                <Row>
                    <Col md={6} className='mb-4'>
                        <Form.Group>
                            <Form.Label>Leave Type</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter leave type'
                            />
                        </Form.Group>
                    </Col>
                     <Col md={6} className="mb-4">
                         <Form.Group>
                            <Form.Label>No of Days:</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter no of days'
                            />
                         </Form.Group>
                     </Col>
                </Row>
                <Button type="submit" onClick={handleLeaveTypes} >submit</Button>
            </Form>
        </Card.Body>
        <Card.Body>
        {showList===true &&(
            <Table 
            responsive
            className='table-centered table-nowrap rounded mb-0'>
                <thead className='thead-light'>
                    <tr>
                        <th>#</th>
                        <th>Leave Types</th>
                        <th>no of days</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </Table>
        )}
        </Card.Body>
    </Card>
  )
}

export default LeaveTypes