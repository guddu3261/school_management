import React from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
const LeaveDetails = () => {
  return (
    <Card className='bg-white shadow-sm mb-4' >
      <Card.Body>
        <h4 className='text-center'>Leave Details</h4>
          <Table responsive className='table-centered table-nowrap mb-0 mt-4' >
              <thead className='thead-light'>
                <tr>
                    <th>#</th>
                    <th>Teacher Name</th>
                    <th>Leave Type</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>No Of Days</th>
                    <th>Reason</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
              </thead>
          </Table>
      </Card.Body>
    </Card>
  )
}

export default LeaveDetails