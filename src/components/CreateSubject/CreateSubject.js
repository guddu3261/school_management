import { Card, Col, Form, Row } from '@themesberg/react-bootstrap'
import React from 'react'

const CreateSubject = () => {

  return (
   <Card border="light" className="bg-white shadow-sm mb-0">
    <Card.Body>
        <Form>
            <Row className="mt-3">
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Select class</Form.Label>
                        <Form.Select>
                            <option>select Class</option>
                            <option>class1</option>
                            <option>class2</option>
                            <option>class3</option>
                            <option>class4</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Select Department</Form.Label>
                        <Form.Select>
                            <option>select Department</option>
                            <option>Science</option>
                            <option>Computer</option>
                            <option>physics</option>
                            <option>Maths</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
        </Form>
    </Card.Body>
   </Card>
  )
}

export default CreateSubject