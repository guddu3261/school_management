import React from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';


const AddAdmin = () => {
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
        <Card.Body>
            <Form>
                <h4 className='text-center' >Add Admin</h4>
                <Row  className='mt-3' >
                    <Col md={6} className='mb-4'>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter name'
                            />
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-4'>
                        <Form.Group>
                            <Form.Label>Select School</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='mb-4'>
                        <Form.Group>
                            <Form.Label>Select Academic Year</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-4'>
                        <Form.Group>
                            <Form.Label>Select Role</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Button type='submit'>submit</Button>
            </Form>
        </Card.Body>
    </Card>
		
  )
}

export default AddAdmin