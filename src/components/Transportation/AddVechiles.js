import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table,Dropdown} from '@themesberg/react-bootstrap';
import axios from 'axios';
import {Formik} from 'formik';
// import { SnackPosition } from '../../shared/constants';
import Snackbar from '@mui/material/Snackbar';
// import { Alert, AlertTitle } from '@material-ui/lab';
import MuiAlert from '@mui/material/Alert';

const AddVechiles = () => {

  const initialValue={
    name:"",
    userName:"",
    email:"",
    mobile:"",

  }

const handleSaveData=(values,{resetForm})=>{
  debugger;
  const busData={
    name:values.name,
    username:values.userName,
    mobile:values.mobile,
    email:values.email,
    enable:values.enable,
    id:values.id,

  }
  resetForm({
    name:"",
    username:"",
    mobile:"",
    email:"",
  })

  axios.post('http://192.168.0.115/kkrtc-bus/server.php/api/saveUpdatesUsers',busData).then((response)=>{
    if(response.data.status){
      console.log(response.data);
      setSnackBarMsg(response.data.message);
      setsavenextmessage(true);
      setFormat("success");
      // resetForm();
      
    }
  }).catch((error)=>{
    setFormat('error');
    setSnackBarMsg('something went wrong')
    console.log('data could not bu saved',error)
  })
}
// Snackbar Message
 const SnackPosition = {
  vertical: 'top',
  horizontal: 'right',
  duration: 3000
}
const [savenextmessage, setsavenextmessage] = React.useState(false);
const [format, setFormat] = React.useState("");
const [snackBarMsg, setSnackBarMsg] = React.useState("");
const closeSuccessToast = () => {
    setsavenextmessage(false)
  }
  return (
    <Card border='light' className='bg-white shadow-sm mb-0' >
       <Snackbar open={savenextmessage} style={{ marginTop: "65px" }} autoHideDuration={SnackPosition.duration} onClose={closeSuccessToast} anchorOrigin={{
               vertical: SnackPosition.vertical,
               horizontal: SnackPosition.horizontal
              }}>
            <MuiAlert onClose={() => { setsavenextmessage(false) }} severity={format} className="snackBar">
              {snackBarMsg}
            </MuiAlert>
          </Snackbar>
      <Card.Body>
        <h4 className='text-center'>Sav bus details</h4>
        <Formik
        initialValues={initialValue}
        onSubmit={handleSaveData}
        >
         {({
          errors,
          values,
          touched,
          handleChange,
          handleSubmit,
          resetForm,
          isSubmitting,
          handleBlur
         })=>{
          return(
            <Form  onSubmit={handleSubmit} >
              <Row>
                <Col md={6} className='mb-4'>
                  <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                       name='name'
                      type='text'
                      placeholder='enter name'
                      value={values.name}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                  <Form.Group>
                    <Form.Label>user name</Form.Label>
                    <Form.Control
                      type='text'
                      name="userName"
                      placeholder='enter username'
                      value={values.userName}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6} className='mb-4'>
                  <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type='text'
                      name="email"
                      placeholder='enter email'
                      value={values.email}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                  <Form.Group>
                    <Form.Label>mobile</Form.Label>
                    <Form.Control
                      type='text'
                      name="mobile"
                      placeholder='enter mobile'
                      value={values.mobile}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Button type='submit' onClick={handleSubmit} >save</Button>
            </Form>
          )
         }}
        </Formik>
      </Card.Body>
    </Card>
  )
}

export default AddVechiles;