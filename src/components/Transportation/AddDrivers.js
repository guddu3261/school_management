import React ,{useEffect}from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table,Dropdown} from '@themesberg/react-bootstrap';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import { faCalendarAlt ,faPlus,faBackward, faPenSquare,faTrash} from '@fortawesome/free-solid-svg-icons';
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
const AddDrivers = () => {
  const[selectDate,setSelectDate]=React.useState("");
  const[inputFieldAdd,setInputFieldAdd]=React.useState([]);
     const[busRegNo,setBusRegno]=React.useState('');
     const[fare,setFare]=React.useState('');
     const[arrivalTime,setArrivalTime]=React.useState('');
     const[departureTime,setDepartureTime]=React.useState('');
     const[busStop,setBusStop]=React.useState('');
const[initialValue,setInitialValue]=React.useState(
  {cityType:"",busRegNo:"",routeName:"",startTime:"",endtime:"",selectDate:""}
)
const handleChange=(e)=>{
  const values1={...initialValue};
  values1[e.target.name] = e.target.value;
  // console.log(values1);
  setInitialValue(values1);
}
const[showTable,setShowTable]=React.useState(true);
const[showAddForm,setShowAddForm]=React.useState(false);
const handleDeleteField=(index)=>{
  const values=[...inputFieldAdd]
    values.splice(index, 1);
    setInputFieldAdd(values);
}
      const handleChangeInput = (index, event) => {
        debugger;
        const values = [...inputFieldAdd];
        values[index][event.target.name] = event.target.value;
        setInputFieldAdd(values);
      }
      const handleAddFields = () => {
        debugger;
       let arr = [...inputFieldAdd];
       arr.push( { busStopName: '', arrivalTimes: '', dapartureTime:'',fares:''});
       setInputFieldAdd(arr);
      }
    const[getTrip,setGetTrip]=React.useState([])
     const handlegetAllTrip=async()=>{
      debugger;
      const base_url="http://192.168.0.115/kkrtc-bus/server.php/api/getAllTrips";
      const data={
      }
        await axios.post(base_url,data).then((response)=>{
           if(response.data.status){
            setGetTrip(response.data.data)
            console.log(response.data)
           }
        }).catch((error)=>{
          console.log("Get all trip error",error)
        })
     }
     const handleAddForm=()=>{
      setShowAddForm(true)
      setShowTable(false)
     }
      useEffect(()=>{
        handlegetAllTrip()
      },[])

      const handleSaveTripList=()=>{
      debugger;
        let wcdata={
          city_type:initialValue.cityType,
          start_time:initialValue.startTime,
          end_time:initialValue.endtime,
          // bus_stop_id:id,
          stops_data:inputFieldAdd
        }
        console.log(wcdata);
        axios.post('http://192.168.0.103/kkrtc-bus/server.php/api/saveUpadtesTripsSchedules',wcdata).then((response)=>{
        if(response.data.status){
           console.log(response.data);
           setSnackBarMsg(response.data.message);
           setsavenextmessage(true);
           setFormat("success");
        }
        }).catch((error)=>{
            setFormat('error');
            setSnackBarMsg('something went wrong')
            console.log("data didn't save" ,error);
        })
      }
      // snackbar message
      const SnackPosition = {
        vertical: 'top',
        horizontal: 'right',
        duration: 3000
      }
      const [savenextmessage, setsavenextmessage] = React.useState(false);
const [format, setFormat] = React.useState("");
const [snackBarMsg, setSnackBarMsg] = React.useState("");
const closeSuccessToast = () => {
    setsavenextmessage(false);
  }  
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
    <Snackbar open={savenextmessage} style={{ marginTop: "65px" }} autoHideDuration={SnackPosition.duration} onClose={closeSuccessToast} anchorOrigin={{
               vertical: SnackPosition.vertical,
               horizontal: SnackPosition.horizontal
              }}>
            <MuiAlert onClose={() => { setsavenextmessage(false) }} severity={format} className="snackBar">
              {snackBarMsg}
            </MuiAlert>
          </Snackbar>
      <Card.Body>
        {/* <h4 className='text-center'>Trip List</h4> */}
        {showAddForm==true &&(
         <>
         <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            {/* <h4>Class</h4> */}
          </div>
          <Dropdown className="btn-toolbar" onClick={() =>{
            setShowAddForm(false);
            setShowTable(true);
          }}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
              >
              <FontAwesomeIcon icon={faBackward} cNewlassName="me-2" />
            </Dropdown.Toggle>
          </Dropdown>
        </div>
          <Form>
          <Row className='mt-3'>
            <Col md={6} className='mb-0'>
              <Form.Group>
                <Form.Label>City type</Form.Label>
                <Form.Select
                name="cityType"
                valve={initialValue.cityType}
                onChange={handleChange}
                >
                  <option>please select</option>
                  <option>Bnegaluru</option>
                  <option>Mengaluru</option>
                  <option>Hyderabad</option>
                  <option>Udupi</option>
                </Form.Select>
              </Form.Group>
            </Col>
               <Col md={6} className="mb-3">
                       <Form.Group>
                       <Form.Label>date</Form.Label>
                       <Datetime
                               timeFormat={false}
                               onChange={setSelectDate}
                               renderInput={(props, openCalendar) => (
                              <InputGroup>
                              <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                              //  required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={selectDate ? moment(selectDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                             </InputGroup>
                       )}/>
                       </Form.Group>  
                    </Col>
          </Row>
          <Row className='mt-2' >
            <Col md={6} className='mb-0'>
              <Form.Group>
                <Form.Label>Bus Registration No</Form.Label>
                <Form.Select
                name="busRegNo"
                value={initialValue.busRegNo}
                 onChange={handleChange}
                >
                  <option>please select</option>
                  <option>KA05HG1887</option>
                  <option>KA03HT1665</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col md={6} className='mb-0'>
              <Form.Label>Select Rote Name</Form.Label>
              <Form.Select
              name="routeName"
              value={initialValue.routeName}
              onChange={handleChange}
              >
                <option>please select</option>
                <option>Mahalakshmi Layot</option>
                <option>RajajiNagar </option>
              </Form.Select>
            </Col>
          </Row>
          <Row  className='mt-2' >
          <Col md={6} className="mb-3">
                       <Form.Group>
                       <Form.Label> Start Time</Form.Label>
                         <Form.Control
                         name='startTime'
                          type='text'
                          value={initialValue.startTime}
                          onChange={handleChange}
                         />
                       </Form.Group>  
                    </Col>
                     <Col md={6} className="mb-3">
                        <Form.Group>
                        <Form.Label> End Time</Form.Label>
                        <Form.Control
                       name='endtime'
                        type='text'
                        value={initialValue.endtime}

                        onChange={handleChange}
                       />
                       </Form.Group>  
                    </Col>
          </Row>
          <Button onClick={handleAddFields}>Add</Button>
          <Button  onClick={handleDeleteField} >Delete</Button>
          { inputFieldAdd.map((item,index)=>(

            <Row className='mt-2' key={index}  >
                 <Col md={4} className='mb-0'>
                  <Form.Group>
                    <Form.Label>Bus Stop Name</Form.Label>
                       <Form.Select
                       name='busStopName'

                       value={inputFieldAdd[index].busStop}
                      //  onChange={e=>setBusStop(e.target.busStopName)}
                       onChange={event => handleChangeInput(index, event)}
                       >
                        <option>please select</option>
                        <option>Majestic</option>
                        <option>yashvantpur</option>
                       </Form.Select>
                  </Form.Group>
                 </Col>

                 <Col md={3} className='mb-0'>
                  <Form.Group>
                    <Form.Label>Arrival Time</Form.Label>
                      <Form.Control
                      name='arrivalTimes'
                        type='text'
                        value={inputFieldAdd[index].arrivalTime}
                        // onChange={e=>setArrivalTime(e.target.value)}
                        placeholder='arrival time'
                        onChange={event => handleChangeInput(index, event)}
                        
                      />
                  </Form.Group>
                 </Col>

                 <Col md={3} className='mb-0'>
                  <Form.Group>
                    <Form.Label>Departure Time</Form.Label>
                      <Form.Control
                      name="dapartureTime"
                        type='text'
                        value={inputFieldAdd[index].departureTime}
                        // onChange={e=>setDepartureTime(e.target.value)}
                        placeholder='departure time'
                        // value={inputFieldAdd.departureTime}
                        onChange={event => handleChangeInput(index, event)}
                      />
                  </Form.Group>
                 </Col>

                 <Col md={2} className='mb-0'>
                  <Form.Group>
                    <Form.Label>Fare</Form.Label>
                      <Form.Control
                      name='fares'
                        type='text'
                        value={inputFieldAdd[index].fare}
                        // onChange={e=>setFare(e.target.value)}
                        placeholder='Fare'
                        onChange={event => handleChangeInput(index, event)}
                      />
                  </Form.Group>
                 </Col>
                 
              </Row>  
          )) }
          <Button  onClick={handleSaveTripList} >submit</Button>
        </Form>
         </>
        )}

       {showTable==true &&(
        <Card.Body>
        <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          
          <Dropdown className="btn-toolbar"
           onClick={() => handleAddForm()}
           >
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faPlus} className="me-2" />
              Create Trips
            </Dropdown.Toggle>
          </Dropdown>
        </div>
          <Table responsive className='table-centered table-nowrap rounded mb-0'>
            <thead className='thead-light'>
              <tr>
                <th className='border=0'>#</th>
                <th className='border=0'>City Type</th>
                <th className='border=0'>Date</th>
                <th className='border=0'>Start Time</th>
                <th className='border=0'>End Time</th>
                <th className='border=0'>Bus no</th>
                {/* <th className='border=0'>Route name</th> */}
                <th className='border=0'>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                getTrip.map((values,i)=>{
                  return(
                    <tr key={i} >
                      <td>{i+1} </td>
                      <td>{values.city_type} </td>
                      <td>{values.date} </td>
                      <td>{values.start_time} </td>
                      <td>{values.end_time} </td>
                      <td>{values.bus_reg_no} </td>
                      {/* <td>{values.route_name} </td> */}
                      <td>
                      <Row>
                    <Col md={2}>
                    <FontAwesomeIcon 
                    className='icon_title'
                    icon={faPenSquare}
                    onClick={()=>{
                      setShowAddForm(true);
                      setShowTable(false);
                    }}
                    />
                    </Col>
                    <Col>
                   
                     <FontAwesomeIcon icon={faTrash}
                    // onClick={()=>{
                    //   setShowAddForm(true);
                    //   setShowTable(false);
                    // }}
                     />
                    </Col>
                  </Row>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </Table>
        </Card.Body>
       )}
      </Card.Body>
    </Card>
  )
}
export default AddDrivers;