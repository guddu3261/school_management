import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table,Dropdown} from '@themesberg/react-bootstrap';
import { faCalendarAlt ,faPlus,faBackward, faPenSquare,faTrash} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const SetRoutes = () => {
  const[tripList,setTripList]=useState([]);
useEffect(()=>{
  getAllTrips();
  
},[])
  const getAllTrips=async()=>{
    debugger;
    const data={}
    const base_url="http://192.168.0.115/kkrtc-bus/server.php/api/getAllTrips";
    await axios.post(base_url,data).then((response)=>{
    if(response.data.status){
      setTripList(response.data.data)
    }
    }).catch((error)=>{
      console.log('get all trips errors',error)
    })
  }
const[busStopName,setBustStopName]=useState([]);
const getBustStop=async(tripId)=>{
  debugger;
  const data={
    
    bus_stand_id :tripId
  
  }
  const base_url=`http://192.168.0.115/kkrtc-bus/server.php/api/getAllBusStands`;
  await axios.post(base_url,data).then((response)=>{
    setBustStopName(response.data.data)
   console.log(response.data.data);
  }).catch((error)=>{
    console.log("get all bus stop error",error);
  })
}

  const[expanded,setExpanded]=useState(false)
  const handleChange=(panel)=>(event,newExpanded)=>{
    debugger;
    setExpanded(newExpanded ? panel : false);
    getBustStop(panel);
  }
  const Accordion = styled((props) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
  ))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
  }));
  const AccordionSummary = styled((props) => (
    <MuiAccordionSummary
      expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
      {...props}
    />
  ))(({ theme }) => ({
    backgroundColor:
      theme.palette.mode === 'dark'
        ? 'rgba(255, 255, 255, .05)'
        : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
      transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
      marginLeft: theme.spacing(1),
    },
  }));
  
  const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
  }));

  return (
<Card className='bg-white shadow-sm' border='light' >
<Card.Body>
<h4 className='text-center' >Trip List</h4>

<Table responsive className='table-centered table-nowrap rounded mb-0'>
    <thead className='thead-light'>
      <tr>
        <th className='border=0' >#</th>
        <th className='border=0' >Bus From</th>
        <th className='border=0' >Bus To</th>
        <th className='border=0' >Start Time</th>
        <th className='border=0' >End Time</th>
        <th className='border=0' >Bus Name</th>
      </tr>
    </thead>
   
    <tbody>
     {
      tripList.map((values,i)=>{
        return(
      <tr>
       <Accordion expanded={expanded===values.bus_stand_id} onChange={handleChange(values.bus_stand_id)}>
        <AccordionSummary aria-controls='panel1d-content' id='panel1d-header'>
        <td>{i+1}</td>
        <td>{values.from_stop}</td>
        <td>{values.to_stop}</td>
        <td>{values.start_time}</td>
        <td>{values.end_time}</td>
        <td>{values.bus_reg_no}</td>
        </AccordionSummary>
        <AccordionDetails>
          <Table responsive className='table-centered table-nowrap rounded mb-0'>
            <thead className='thead-light'>
              <tr>
                <th>#</th>
                <th>bus Stop</th>
                <th>Arrival Timing</th>
              </tr>
            </thead>
            <tbody>
              {
                busStopName.map((values,i)=>{
                  return(
                    <tr key={i} >
                      <td>{i+1} </td>
                      <td>{values.bus_stand_name} </td>
                      <td>{values.arrival_time} </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </Table>
        </AccordionDetails>
       </Accordion>
      </tr>
        )
      })
     }
    
      </tbody>
      
  </Table>
  
</Card.Body>
</Card>
  )
}

export default SetRoutes;