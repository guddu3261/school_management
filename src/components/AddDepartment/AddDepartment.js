import { Card, Col, Form, Row,Button } from '@themesberg/react-bootstrap'
import React from 'react'

const AddDepartment = () => {
  return (
   <Card border="light" className="bg-white shadow-sm mb-0">
    <Card.Body>
        <h6 className='text-center mb-4' >Add Department</h6>
        <Form>
            <Row className="mt-3">
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Select Department</Form.Label>
                        <Form.Select>
                            <option>select department</option>
                            <option>department 1</option>
                            <option>department 2</option>
                            <option>department 3</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className="mb-0">
                    <Form.Group>
                        <Form.Label>Department code</Form.Label>
                          <Form.Control
                            type="text"
                            placeholder='enter department code'
                          />
                    </Form.Group>
                </Col>
            </Row>
            <Button className='mt-4'>Submit</Button>
        </Form>
    </Card.Body>
   </Card>
  )
}

export default AddDepartment;