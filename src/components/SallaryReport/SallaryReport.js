import React from 'react';
import {Col,Row,Form,Button,Card} from '@themesberg/react-bootstrap'

const SallaryReport = () => {
  return (
   <Card border='light' className='bg-white shadow-sm mb-0'>
    <Card.Body>
        <h5 className='text-center'>Salary Report</h5>
        <Row className='mt-4' >
        <Col className='mb-4' md={6} >
                    <Form.Group>
                        <Form.Label>Select Teacher </Form.Label>
                         <Form.Select>
                            <option>please select</option>
                             <option>Teacher 1</option>
                             <option>Teacher 2</option>
                             <option>Teacher 3</option>
                         </Form.Select>
                    </Form.Group>
                </Col>
                <Col className='mb-4' md={6} >
                    <Form.Group>
                        <Form.Label>Select Month </Form.Label>
                         <Form.Select>
                            <option>please select</option>
                             <option>January</option>
                             <option>February</option>
                             <option>March</option>
                             <option>April</option>
                             <option>May</option>
                             <option>June</option>
                         </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label></Form.Label>
                    </Form.Group>
                </Col>
        </Row>
    </Card.Body>
   </Card>
  )
}

export default SallaryReport