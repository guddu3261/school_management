import React,{useState,useEffect} from 'react';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarAlt } from '@fortawesome/free-solid-svg-icons';

const AddHoliday = () => {
     const[showAddForm,setShowAddForm]=useState(true);
     const[showList,setShowList]=useState(false);
    const[startHolidayDate,setStartHolidayDate]=useState("");
    const[endHolidayDate,setEndHolidayDate]=useState("");
    const handleAddHoliday=()=>{
        setShowList(true)
        setShowAddForm(true)
    }
  return (
    <Card className='bg-white shadow-sm mb-4'>
        <Card.Body>
           {showAddForm==true &&(
            <>
            <Row>
                <h4 className='text-center' >Add Holiday Details</h4>
            </Row>
            <Form>
                    <Row>
                        <Col md={6} className='mb-3 mt-3'>
                            <Form.Group>
                                <Form.Label>Select From Date:</Form.Label>
                                <Datetime
                           timeFormat={false}
                           onChange={setStartHolidayDate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={startHolidayDate ? moment(startHolidayDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                             </InputGroup>
                               )} />
                            </Form.Group>
                        </Col>
                        <Col md={6} className="mb-3 mt-3">
                            <Form.Group>
                                <Form.Label>Select To Date:</Form.Label>
                                <Datetime
                           timeFormat={false}
                           onChange={setEndHolidayDate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={endHolidayDate ? moment(endHolidayDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                             </InputGroup>
                           )} />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                    <Col md={6} className='mb-3 mt-3'>
                            <Form.Group>
                                <Form.Label>Select Type</Form.Label>
                                  <Form.Select>
                                    <option>please select</option>
                                    <option>Teacher</option>
                                    <option>Student</option>
                                  </Form.Select>
                            </Form.Group>
                        </Col>
                        <Col md={6} className="mb-3 mt-3">
                            <Form.Group>
                                <Form.Label>Holiday Name:</Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='enter holiday name'
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                        <Button type='submit' className=" justify-content-between"  onClick={handleAddHoliday} >submit</Button>
            </Form>
            </>
           )}
        </Card.Body>
        <Card.Body>
            {showList==true &&(
                <Table responsive className="table-centered table-nowrap rounded mb-0" >
               <thead>
                 <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Action</th>
                 </tr>
               </thead>
            </Table>
            )}
        </Card.Body>
    </Card>
  )
}

export default AddHoliday