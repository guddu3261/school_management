import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';

const AddPreviousDate = () => {
    const[showAddForm,setShowAddForm]=useState(true);
    const[showList,setShowList]=useState(false);

    const handlePreviousDate=()=>{
        setShowList(true);
        setShowAddForm(true)
    }
  return (
   <Card border="light" className="bg-white shadow-sm mb-4">
    <Card.Body>
       {showAddForm==true &&(
        <>
        <h4 className='text-center' >Enter Previous Dates Details</h4>
        <Form>
            <Row>
                <Col md={6} className='mt-4'>
                    <Form.Group>
                        <Form.Label>Select Class</Form.Label>
                         <Form.Select>
                            <option>please select</option>
                            <option>class 1</option>
                            <option>class 2</option>
                            <option>class 3</option>
                            <option>class 4</option>
                            <option>class 5</option>
                            <option>class 6</option>
                         </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4 mt-4'>
                    <Form.Group>
                        <Form.Label>No Of Days:</Form.Label>
                         <Form.Control
                            type='text'
                            placeholder='No Of Days'
                         />
                    </Form.Group>
                </Col>
            </Row>
            <Button type='submit'onClick={handlePreviousDate}>submit</Button>
        </Form>
        </>
       )}
    </Card.Body>
    <Card.Body>
        {showList==true &&(
            <Table response className='table-centered table-nowrap rounded mb-0' >
            <thead>
                <tr>
                   <th>#</th>
                   <th>Class Name</th>
                   <th>No Of Days</th>
                   <th>Action</th>
                </tr>
            </thead>
        </Table>
        )}
    </Card.Body>
   </Card>
  )
}

export default AddPreviousDate;