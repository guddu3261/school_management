import React from 'react';
import{Row,Col,Button,Form,Card} from '@themesberg/react-bootstrap';

const StudentList = () => {
  return (
    <Card border='light' className='bg-white shadow-sm mb-4'>
        <Card.Body>
            <Form>
                <Row>
                    <Col md={6} className='mb-3' >
                        <Form.Group>
                            <Form.Label>Select class</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                                <option>class 1</option>
                                <option>class 2</option>
                                <option>class 3</option>
                                <option>class 4</option>
                                <option>class 5</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select section</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                                <option>section A</option>
                                <option>section B</option>
                                <option>section C</option>
                                <option>section D</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Button type="submit">Submit</Button>
            </Form>
        </Card.Body>
    </Card>
  )
}

export default StudentList