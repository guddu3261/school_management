import React,{useState,useEffect} from 'react';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarAlt } from '@fortawesome/free-solid-svg-icons';

const StudentAttendanceUpdate = () => {
   const[startDate,setStartDate]=useState("");
   const[attendanceDate,setAttendanceDate]=useState("");
    const[endDate,setEndDate]=useState("");
    const[showList,setShowList]=useState(false);
    const[showAddForm,setShowAddForm]=useState(true);
   

    const showAttendance=()=>{
        setShowList(true);
        setShowAddForm(true);
    }
  return (
    <Card border='light' className='bg-white shadow-sm mb-4' >
        <Card.Body>
           {showAddForm==true &&(
            <Form>
            <Row className='mt-3' >
                    <Col md={4} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select class</Form.Label>
                              <Form.Select>
                                <option>please select</option>
                                <option>class  1</option>
                                <option>class  2</option>
                                <option>class  3</option>
                                <option>class  4</option>
                                <option>class  5</option>
                              </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={4} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select section</Form.Label>
                              <Form.Select>
                                <option>please select</option>
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                              </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={4} className="mb-3">
                       <Form.Group>
                       <Form.Label> Select date</Form.Label>
                       <Datetime
                           timeFormat={false}
                           onChange={setAttendanceDate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={attendanceDate ? moment(attendanceDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                    </InputGroup>
                  )} />
                       </Form.Group>
                           
                    </Col>
                </Row>
                <Button type='submit' onClick={showAttendance} >Delete</Button>
            </Form>
           )}
        </Card.Body>

        <Card.Body>
            {showList==true &&(
                <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Id</th>
                  <th className="border-0">Student Name</th>
                  <th className="border-0">Admission Number</th>
                  <th className="border-0">Status</th>
                  <th className="border-0">Action</th>
                  
                </tr>
              </thead>
              </Table>
            )}
        </Card.Body>
    </Card>
  )
}

export default StudentAttendanceUpdate;