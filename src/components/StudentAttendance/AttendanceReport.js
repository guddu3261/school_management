import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';

const AttendanceReport = () => {
    const[months,setMonths]=useState(
        [
            {id:1,Month:"January",Class:"LKG"},
            {id:2,Month:"February",Class:"UKG"},
            {id:3,Month:"March",Class:"1st"},
            {id:4,Month:"April",Class:"2nd"},
            {id:5,Month:"May",Class:"3rd"},
            {id:6,Month:"June",Class:"4th"},
            {id:7,Month:"July",Class:"5th"},
            {id:8,Month:"August",Class:"6th"},
            {id:9,Month:"September",Class:"7th"},
            {id:10,Month:"October",Class:"8th"},
            {id:11,Month:"November",Class:"9th"},
            {id:12,Month:"December",Class:"10th"},
        ]
    )
  return (
   <Card>
    <Card.Body>
        <Form>
            <Row>
                <Col className='mb-3' md={3}>
                    <Form.Group>
                        <Form.Label>Select year</Form.Label>
                          <Form.Select>
                            <option>please select</option>
                             <option>2016-2017</option>
                             <option>2017-2018</option>
                             <option>2019-2020</option>
                             <option>2020-2021</option>
                             <option>2021-2022</option>
                             <option>2022-2023</option>
                          </Form.Select>
                    </Form.Group>
                </Col>
                <Col className='mb-3' md={3}>
                    <Form.Group>
                        <Form.Label>Select Month</Form.Label>
                          <Form.Select>
                            <option>please select</option>
                            {
                                    months.map((item)=>(
                                        <option >{item.Month}</option>
                                    ))
                                }
                          </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={3} className='md-3'>
                    <Form.Group>
                        <Form.Label>Select class</Form.Label>
                        <Form.Select>
                           <option>please select</option>
                           {
                                    months.map((item)=>(
                                        <option >{item.Class}</option>
                                    ))
                                }
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={3} className='md-3'>
                    <Form.Group>
                        <Form.Label>Select section</Form.Label>
                        <Form.Select>
                           <option>please select</option>
                             <option>A</option>
                             <option>B</option>
                             <option>C</option>
                             <option>D</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
            <Button type='submit'>Submit</Button>
        </Form>
    </Card.Body>
   </Card>
  )
}

export default AttendanceReport