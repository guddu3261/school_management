import React,{useState,useEffect} from 'react';
import{Row,Col,Card,Button,InputGroup,Form} from '@themesberg/react-bootstrap';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarAlt } from '@fortawesome/free-solid-svg-icons';

const StudentAttendanceEntry = () => {
    const[attendanceDate,setAttendancedate]=useState('');
    const[classValue,setClassValue]=useState("");
    const[section,setSection]=useState("");
  return (
    <Card className='bg-white shadow-sm mb-7'>
        <Card.Body>
             <Row>
               <Col md={4} className='mb-3'>
                   <Form.Group>
                     <Form.Label>Select class</Form.Label>
                           <Form.Select
                           name='classValue'
                           value={classValue}
                           onChange={(e)=>{
                            setClassValue(e.target.value)
                           }}
                           >
                                <option>please select</option>
                                <option>class  1</option>
                                <option>class  2</option>
                                <option>class  3</option>
                                <option>class  4</option>
                                <option>class  5</option>
                              </Form.Select>
                   </Form.Group>
                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                    <Form.Label>Select section</Form.Label>
                          <Form.Select
                          name="section"
                          value={section}
                          onChange={(e)=>{
                            setSection(e.target.value)
                          }}
                          >
                             <option>please select</option>
                             <option>A</option>
                             <option>B</option>
                             <option>C</option>
                             <option>D</option>
                             <option>E</option>
                          </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Select date</Form.Label>
                        <Datetime
                           timeFormat={false}
                           onChange={setAttendancedate}
                           renderInput={(props, openCalendar) => (
                          <InputGroup>
                           <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                             <Form.Control
                               required
                              // value={values.dob}
                               // onChange={handleChange}
                               value={attendanceDate ? moment(attendanceDate).format("MM/DD/YYYY") : ""}
                               placeholder="mm/dd/yyyy"
                               onFocus={openCalendar}
                               onChange={() => { }}
                                />
                    </InputGroup>
                  )} />
                    </Form.Group>
                </Col>
             </Row>
             <Button type='submit' >submit</Button>
        </Card.Body>
    </Card>
  )
}

export default StudentAttendanceEntry