import React ,{useState,useEffect}from 'react';
import {Row,Col,Button,Form,Card} from '@themesberg/react-bootstrap';


const StudentPasswordSetting = () => {
    const[classNameValue,setClassNameValue]=useState("")
    const[section,setSection]=useState("");
    const[student,setStudent]=useState("");
    const[userName,setUserName]=useState("");
    const[password,setPassword]=useState("")
  return (
    <Card className='bg-white shadow-sm mb-3' border='light' >
    <Card.Body>
        <Form>
            <Row>
                <Col md={4} className='mb-3' >
                  <Form>
                    <Form.Group>
                        <Form.Label>Select class</Form.Label>
                          <Form.Select
                          value={classNameValue}
                          onChange={(e)=>{
                            setClassNameValue(e.target.value)
                          }}
                          >
                            <option>please select</option>
                            <option>Class 1</option>
                            <option>Class 2</option>
                            <option>Class 3</option>
                            <option>Class 4</option>
                          </Form.Select>
                    </Form.Group>
                  </Form>

                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Select section</Form.Label>
                           <Form.Select
                           value={section}
                           onChange={(e)=>{
                            setSection(e.target.value)
                            console.log(e.target.value)
                           }}
                           >
                             <option>please select</option>
                             <option>section A</option>
                             <option>section B</option>
                             <option>section C</option>
                             <option>section D</option>
                           </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={4}>
                    <Form.Group>
                        <Form.Label>Select student</Form.Label>
                        <Form.Select
                        value={student}
                        onChange={(e)=>{
                            setStudent(e.target.value)
                        }}
                        >
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
            <Row className='mt-3' >
                <Col md={6} className='mb-3'>
                    <Form.Group>
                        <Form.Label>username</Form.Label>
                        <Form.Control
                            name="userName"
                            value={userName}
                            onChange={(e)=>{
                                setUserName(e.target.value)
                            }}
                            type='text'
                            placeholder='enter username'
                        />
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type='text'
                            name=''
                            value={password}
                            onChange={(e)=>{
                                setPassword(e.target.value)
                            }}
                            placeholder=' enter password'
                        />
                    </Form.Group>
                </Col>
            </Row>
             <Button>Submit</Button>
        </Form>
    </Card.Body>

    </Card>
  )
}

export default StudentPasswordSetting