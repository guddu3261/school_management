import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import './Class.css'

import {
  faCashRegister,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faPenSquare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  Form,
} from "@themesberg/react-bootstrap";
import grayback from '../../assets/img/icons/grayback.png'
// import Tooltip from '@mui/material/Tooltip'
const Teachers = () => {
  const [showAddForm, setShowAddForm] = useState(false);
  const [showTable, setShowTable] = useState(true);
  const [loaderFlag, setLoaderFlag] = useState(true);
  const handleAddForm = () => {
    setShowAddForm(true);
    setShowTable(false);
  };

  const [teacherArray, setTeacherArray] = useState([
    { teacherArray: "John", className: "First Standard"},
    { teacherArray: "Smith", className: "Second Standard"},
    { teacherArray: "Joe Denil", className: "Third Standad"},
    { teacherArray: "Daniel", className: "Fourth Standard"},
    { teacherArray: "JackSon", className: "Fifth Standard"},
    { teacherArray: "Sunil", className: "Sixth Standard" },
  ]);
  const[teachers,setTeachers]=useState([
    {teacher:"John"},
    {teacher:"Smith"},
    {teacher:"Singh"},
    {teacher:"shame"},
  ])

  
  return (
    <>
      {showTable == true && (
        <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            <h4>Teachers</h4>
          </div>
          <Dropdown className="btn-toolbar" onClick={() => handleAddForm()}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faPlus} cNewlassName="me-2" />
              Add Class-Teacher
            </Dropdown.Toggle>
          </Dropdown>
        </div>
      )}
      {/* Form Start */}
      {showAddForm == true && (
        <>
          <h4 className="mb-4 mt-4">Teacher</h4>
          
          <Card className="bg-white shadow-sm mb-7" border="light">
            <Card.Body>
              <Row>
              <Col sm={4} className="mb-3">
              <Form.Group className="mb-2">
                <Form.Label>Select Class-Teacher</Form.Label>
                <Form.Select id="state" defaultValue="0">
                  <option value="0">Class-Teacher</option>
                  <option value="AL">Ms.Prema</option>
                  <option value="AK">Mr.Raju</option>
                  <option value="AZ">Ms.Kavya</option>
                  </Form.Select>
              </Form.Group>
            </Col>
          
            <Col sm={4} className="mb-3">
              <Form.Group className="mb-2">
                <Form.Label>Select Class-Name</Form.Label>
                <Form.Select id="state" defaultValue="0">
                  <option value="0">Class-Name</option>
                  <option value="AL">Sixth Standard</option>
                  <option value="AK">Second Standard</option>
                  <option value="AZ">Third Standard</option>
                  </Form.Select>
              </Form.Group>
            </Col>
              </Row>
              <div className="mt-3">
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </div>
            </Card.Body>
          </Card>
        </>
      )}
      {showTable == true && (
        <Card className="shadow-sm mb-4" border="light">
          <Card.Body className="pb-0">
            <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Class-Teacher</th>
                  <th className="border-0">Class-Name</th>
                  <th className="border-0">Action</th>
                  {/* <th className='border-0'>Slno.</th> */}
                </tr>
              </thead>
              <tbody>
                {teacherArray.map((item,i) => (
                  <tr key ={i}>
                    <td>{i+1}</td>
                    <td>{item.teacherArray}</td>
                    <td>{item.className}</td>
                    <td>
                    <Row>
                    <Col md={1}>
                    <FontAwesomeIcon 
                    className='icon_title'
                    icon={faPenSquare}
                    onClick={()=>{
                      setShowAddForm(true);
                      setShowTable(false);
                    }}
                    />
                    </Col>
                    <Col>
                   
                     <FontAwesomeIcon icon={faTrash}
                    // onClick={()=>{
                    //   setShowAddForm(true);
                    //   setShowTable(false);
                    // }}
                     />
                    </Col>
                  </Row>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      )}
    </>
  );
};

export default Teachers;
