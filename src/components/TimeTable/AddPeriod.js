import React from 'react';
import{Row,Col,Button,Form,Card,Table} from '@themesberg/react-bootstrap'

const AddPeriod = () => {
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
        <Card.Body>
            <h4 className='text-center' >Enter period Details</h4>
            <Form>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Period Timing</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter period timing'
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Button type='submit' >save</Button>
            </Form>
        </Card.Body>
        <Card.Body>
            <Table responsive className='table-centered table-nowrap rounded mb-0'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Period Name </th>
                        <th>Action</th>
                    </tr>
                </thead>
            </Table>
        </Card.Body>
    </Card>
  )
}

export default AddPeriod;