import React ,{useState,useEffect}from 'react';
import {Row,Col,Button,Form,Card} from '@themesberg/react-bootstrap';


const TeacherPasswordSetting = () => {
    const[Teacher,setTeacher]=useState("");
    const[userName,setUserName]=useState("");
    const[password,setPassword]=useState("")
  return (
    <Card className='bg-white shadow-sm mb-3' border='light' >
    <Card.Body>
        <Form>
            <Row>
                <Col md={6} className='mb-3' >
                  <Form>
                    <Form.Group>
                        <Form.Label>Select Teacher</Form.Label>
                          <Form.Select
                          value={Teacher}
                          onChange={(e)=>{
                            setTeacher(e.target.value)
                          }}
                          >
                            <option>please select</option>
                            <option>Teacher 1</option>
                            <option>Teacher 2</option>
                            <option>Teacher 3</option>
                            <option>Teacher 4</option>
                          </Form.Select>
                    </Form.Group>
                  </Form>

                </Col>
                  <Col md={6} className='mb-3'>
                    <Form.Group>
                        <Form.Label>username</Form.Label>
                        <Form.Control
                            name="userName"
                            value={userName}
                            onChange={(e)=>{
                                setUserName(e.target.value)
                            }}
                            type='text'
                            placeholder='enter username'
                        />
                    </Form.Group>
                 </Col>
            </Row>
            <Row className='mt-3' >
             
                <Col md={6} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type='text'
                            name=''
                            value={password}
                            onChange={(e)=>{
                                setPassword(e.target.value)
                            }}
                            placeholder=' enter password'
                        />
                    </Form.Group>
                </Col>
            </Row>
             <Button>Submit</Button>
        </Form>
    </Card.Body>

    </Card>
  )
}

export default TeacherPasswordSetting