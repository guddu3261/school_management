import React ,{useState,useEffect}from 'react'
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
const FeeHead = () => {
    const[showAddForm,setShowAddForm]= useState(true);
    const[showList,setShowList]=useState(false);
    const handleFeeHead=()=>{
        setShowList(true);
        setShowAddForm(true)
    }
  return (
   <Card border='light' className='bg-white shadow-sm mb-0'>
    <Card.Body>
       {showAddForm==true &&(
        <>
        <h4 className='text-center' >Fee Head</h4>
        <Form>
            <Row>
                <Col md={6} className='mb-4 mt-3 '>
                    <Form.Group>
                        <Form.Label>Select Fee Group</Form.Label>
                        <Form.Select>
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4 mt-3 '>
                    <Form.Group>
                        <Form.Label>Head Name:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='head name'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
            <Col md={6} className='mb-4 mt-3 '>
                    <Form.Group>
                        <Form.Label>Head Type:</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='head type'
                        />
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4 mt-3 '>
                    <Form.Group>
                        <Form.Label>Amount</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='amount'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Button type='text' onClick={handleFeeHead} >save</Button>
        </Form>
        </>
       )}
    </Card.Body>
    <Card.Body>
        {showList==true &&(
            <Table responsive className='table-centered table-nowrap rounded mb-0'>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Group Name</th>
                    <th>Head Name</th>
                    <th>Head Type</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
        </Table>
        )}
    </Card.Body>
   </Card>
  )
}

export default FeeHead