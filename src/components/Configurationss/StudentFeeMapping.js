import React,{useState,useEffect} from 'react'
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
const StudentFeeMapping = () => {
    const[showAddForm,setShowAddForm]=useState(true)
    const[showList,setShowList]=useState(false)
    const handleFeeMapping=()=>{
        setShowAddForm(true)
        setShowList(true)
    }
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
        <Card.Body>
           {showAddForm==true &&(
            <Form>
            <h4 className='text-center' >Student Fee Mapping</h4>
            <Row>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Select Class</Form.Label>
                        <Form.Select>
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Select Section</Form.Label>
                        <Form.Select>
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Select Student</Form.Label>
                        <Form.Select>
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Select Fee Head</Form.Label>
                        <Form.Select>
                            <option>please select</option>
                        </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
            <Button type='submit' onClick={handleFeeMapping} >submit</Button>
            </Form>
           )}
        </Card.Body>
        <Card.Body>
            {showList==true &&(
                <Table responsive className='table-centered table-nowrap rounded mb-0'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Class Name</th>
                        <th>Section Name</th>
                        <th>Student Name</th>
                        <th>Head Name</th>
                        <th>Total Amount</th>
                        <th>Received</th>
                        <th>Remaining</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </Table>
            )}
        </Card.Body>
    </Card>
  )
}

export default StudentFeeMapping