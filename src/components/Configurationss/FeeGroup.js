import React,{useState,useEffect} from 'react'
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
const FeeGroup = () => {
    const[showAddForm,setShowAddForm]=useState(true);
    const[showList,setShowList]=useState(false);
    const handleFeeGroup=()=>{
        setShowAddForm(true);
        setShowList(true)
    }
  return (
    <Card border='light' className='bg-white shadow-sm mb-4'>
        <Card.Body>
          {showAddForm==true &&(
        <>
        <h4 className='text-center'>Fee Group</h4>
         <Form>
         <Row>
            <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>Group Name:</Form.Label>
                     <Form.Control
                        type='text'
                        placeholder='group name'
                     />
                </Form.Group>
            </Col>
            <Col className='mb-4' md={4}>
                <Form.Group>
                    <Form.Label>Group Type:</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='group type'
                    />
                </Form.Group>
            </Col>
            <Col md={4} className='mb-4'>
                <Form.Group>
                    <Form.Label>Amount:</Form.Label>
                    <Form.Control
                        type='text'
                        placeholder='amount'
                    />
                </Form.Group>
            </Col>
           </Row>
           <Button type='text' onClick={handleFeeGroup} >submit</Button>
         </Form>
        </>
          )}
        </Card.Body>
         <Card.Body>
           { showList==true &&(
            <Table responsive className='table-centered table-nowrap mb-0'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Group Name</th>
                        <th>Group Type</th>
                        <th>Group Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </Table>
           )}
         </Card.Body>
    </Card>
  )
}

export default FeeGroup