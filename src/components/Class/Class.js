import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import './Class.css'
import {
  faCashRegister,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faPenSquare,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  Form,
} from "@themesberg/react-bootstrap";
import grayback from '../../assets/img/icons/grayback.png'
// import Tooltip from '@mui/material/Tooltip'
const Class = () => {
  const [showForm, setShowForm] = useState(false);
  const [showTable, setShowTable] = useState(true);
  const [loaderFlag, setLoaderFlag] = useState(true);
  const handleAddForm = () => {
    setShowForm(true);
    setShowTable(false);
  };

  const [classArray, setClassArray] = useState([
    { className: "LKG",},
    { className: "UKG"},
    { className: "First standard"},
    { className: "Second standard"},
    { className: "Third standard" },
    { className: "Fourth standard" },
  ]);
//   const[teachers,setTeachers]=useState([
//     {teacher:"John"},
//     {teacher:"Smith"},
//     {teacher:"Singh"},
//     {teacher:"shame"},
//   ])

  
  return (
    <>
      {showTable == true && (
        <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            <h4>Class</h4>
          </div>
          <Dropdown className="btn-toolbar" onClick={() => handleAddForm()}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faPlus} cNewlassName="me-2" />
             Create Class
            </Dropdown.Toggle>
          </Dropdown>
        </div>
      )}
      {/* Form Start */}
      {showForm == true && (
        <>
          <h4 className="mb-4 mt-4">Teacher</h4>
          
          <Card className="bg-white shadow-sm mb-7" border="light">
            <Card.Body>
              <Row>
                <Col className="mb-3" md={6}>
                <Form.Group className="mb-2">
                <Form.Label>Select Class-Name</Form.Label>
                <Form.Select id="state" defaultValue="0">
                  <option value="0">Class Name</option>
                  <option value="AL">First Standard</option>
                  <option value="AK">Second Standard</option>
                  <option value="AZ">Third Standard</option>
                  </Form.Select>
              </Form.Group>
                </Col>
              
              </Row>
              <div className="mt-3">
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </div>
            </Card.Body>
          </Card>
        </>
      )}

      {showTable == true && (
        <Card className="shadow-sm mb-4" border="light">
          <Card.Body className="pb-0">
            <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Class</th>
                  <th className="border-0">Action</th>
                  {/* <th className='border-0'>Slno.</th> */}
                </tr>
              </thead>
              <tbody>
                {classArray.map((item,i) => (
                  <tr key ={i}>
                    <td>{i}</td>
                    <td>{item.className}</td>
                    <td>Edit</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      )}
    </>
  );
};

export default Class;
