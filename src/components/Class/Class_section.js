import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import './Class.css'
import {
  faCashRegister,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faPenSquare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  Form,
} from "@themesberg/react-bootstrap";
import grayback from '../../assets/img/icons/grayback.png'
// import Tooltip from '@mui/material/Tooltip'
const Class = () => {
  const[showAddForm,setShowAddForm]=useState(true);
  const [showTable, setShowTable] = useState(false);
  const [loaderFlag, setLoaderFlag] = useState(true);
  const handleAddForm = () => {
    setShowAddForm(true);
    setShowTable(false);
  };

  const [classArray, setClassArray] = useState([
    {id:1, className1: "LKG",section:"LA"},
    {id:2, className1: "UKG",section:"UA"},
    {id:3, className1: "First standard",section:"1A"},
    { id:4,className1: "Second standard",section:"2A"},
    { id:5,className1: "Third standard",section:"3C" },
    { id:6,className1: "Fourth standard",section:"4A" },
  ]);
//   const[teachers,setTeachers]=useState([
//     {teacher:"John"},
//     {teacher:"Smith"},
//     {teacher:"Singh"},
//     {teacher:"shame"},
//   ])

const addDataInTable=()=>{
  debugger;
   let tempArr =[...classArray];
   let id =1;
   if(tempArr.length>0){
   id=tempArr.length+1;
   }
   let tempObj ={
      id : id,
      className1 : classValue,
      section:sectionValue
   }
   tempArr.push(tempObj);
   setClassArray(tempArr);
   console.log(tempArr);
}
const [classValue,setClassValue] =useState("");
const[sectionValue,setSectionValue]=useState("");
  return (
    <>
      {/* {showTable == true && (
        <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-item-center py-4">
          <div className="d-block mb-4 mb-xl-0">
            <h4>Class</h4>
          </div>
          <Dropdown className="btn-toolbar" onClick={() => handleAddForm()}>
            <Dropdown.Toggle
              as={Button}
              variant="primary"
              size="sm"
              className="me-5"
            >
              <FontAwesomeIcon icon={faPlus} cNewlassName="me-2" />
             Create Class
            </Dropdown.Toggle>
          </Dropdown>
        </div>
      )} */}
      {/* Form Start */}
      {showAddForm == true && (
        <>
          <h4 className="mb-4 mt-4">Teacher</h4>
          
          <Card className="bg-white shadow-sm mb-7" border="light">
            <Card.Body>
              <Row>
                <Col className="mb-3" md={6}>
                <Form.Group className="mb-2">
                <Form.Label>Select Class-Name</Form.Label>
                <Form.Select
                value={classValue}
                onChange={(e)=>{
                    setClassValue(e.target.value)
                }}
                >
                <option>Select Class</option>
                {
                    classArray.map((item)=>(
                        <option>{item.className1} </option>
                    ))
                }
                </Form.Select>
                {/* <Form.Control
                  name="className"
                  type="text"
                  value ={classValue}
                  onChange ={(e)=>{
                    debugger;
                    setClassValue(e.target.value);
                  }
                  }
                /> */}
                {/* <Form.Select id="state" defaultValue="0">
                  <option value="0">Class Name</option>
                  <option value="AL">First Standard</option>
                  <option value="AK">Second Standard</option>
                  <option value="AZ">Third Standard</option>
                  </Form.Select> */}
              </Form.Group>
                </Col>
                <Col className="mb-3" md={6}>
                    <Form.Group className="mb-2">
                         <Form.Label>Select Section</Form.Label>
                         <Form.Select
                         value={sectionValue}
                         onChange={(e)=>{
                            setSectionValue(e.target.value)
                         }}
                         >
                         <option>Select Section</option>
                        {
                            classArray.map((iteam)=>(
                                <option>
                                    {iteam.section}
                                </option>
                            ))
                           }
                       
                         </Form.Select>
                    </Form.Group>
                </Col>
              
              </Row>
              <div className="mt-3">
                <Button variant="primary" type="submit" style={{float:"right"}}
                onClick={()=>{
                  setShowTable(true);
                  addDataInTable();
                  // setShowAddForm(false)
                }}
                >
                  Submit
                </Button>
              </div>
            </Card.Body>
          </Card>
        </>
      )}

      {showTable == true && (
        <Card className="shadow-sm mb-4" border="light">
          <Card.Body className="pb-0">
            <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Class</th>
                  <th className="border-0">Section</th>
                  <th className="border-0">Action</th>
                  {/* <th className='border-0'>Slno.</th> */}
                </tr>
              </thead>
              <tbody>
                {classArray.map((item,i) => (
                  <tr key ={i+1}>
                    <td>{i+1}</td>
                    <td>{item.className1}</td>
                    <td>{item.section}</td>
                    <td>
                    <Row>
                    <Col md={1}>
                    <FontAwesomeIcon 
                    className='icon_title'
                    icon={faPenSquare}
                    onClick={()=>{
                      setShowAddForm(true);
                      setShowTable(false);
                    }}
                    />
                    </Col>
                    <Col>
                   
                     <FontAwesomeIcon icon={faTrash}
                    // onClick={()=>{
                    //   setShowAddForm(true);
                    //   setShowTable(false);
                    //   resetForm();
                    // }}
                     />
                    </Col>
                  </Row>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      )}
    </>
  );
};

export default Class;
