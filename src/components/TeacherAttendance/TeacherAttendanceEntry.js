import React,{useState,useEffect} from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarAlt ,faClock} from '@fortawesome/free-solid-svg-icons';
const TeacherAttendanceEntry = () => {
    const[attendanceDate,setAttendance]=useState("")
  return (
   <Card>
    <Card.Body>
        <Form>
            <Row>
            <Col md={4} className='mb-3'>
                <Form.Group>
                    <Form.Label> Select Teacher</Form.Label>
                    <Form.Select>
                        <option>please select</option>
                        <option>Teacher 1</option>
                        <option>Teacher 2</option>
                        <option>Teacher 3</option>
                        <option>Teacher 4</option>
                        <option>Teacher 5</option>
                    </Form.Select>
                </Form.Group>
            </Col>
            <Col md={4}  className='mb-3'>
               <Form.Group>
                <Form.Label>Select Attendance</Form.Label>
                <Form.Select>
                <option>please select</option>
                <option>Present</option>
                <option>Absent</option>
                </Form.Select>
               </Form.Group>
            </Col>
            <Col md={4} className='mb-3'>
                <Form.Group>
                    <Form.Label>Select date</Form.Label>
                    <Datetime
                  timeFormat={false}
                  onChange={setAttendance}
                  renderInput={(props, openCalendar) => (
                    <InputGroup>
                      <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                      <Form.Control
                        required
                        // value={values.dob}
                        // onChange={handleChange}
                        value={attendanceDate ? moment(attendanceDate).format("MM/DD/YYYY") : ""}
                        placeholder="mm/dd/yyyy"
                        onFocus={openCalendar}
                        onChange={() => { }} />
                    </InputGroup>
                  )} />
                </Form.Group>
            </Col>
            </Row>
            <Row>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>In time</Form.Label>
                          <Form.Control
                            type='time'
                          />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-3'>
                    <Form.Group>
                        <Form.Label>Out time</Form.Label>
                        <Form.Control
                            type='time'
                            icon={faClock}
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Button type='submit'>Submit</Button>
        </Form>
    </Card.Body>
   </Card>
  )
}

export default TeacherAttendanceEntry