import React,{useState,useEffect} from 'react';
import{Row,Col,Card,Co,Container,Button,Form} from '@themesberg/react-bootstrap';

const CreateSemester = () => {
const[semester,setSemester]=useState("");
const[semCode,setSemCode]=useState("");

  return (
    <Card className='bg-white shadow-sm mb-4 mt-3'>
     <Card.Body>
      <Form  >
        <Row>
          <Col className='mb-3' md={6}>
            <Form.Group>
              <Form.Label>Enter Semester</Form.Label>
              <Form.Control
                value={semester}
                onChange={(e)=>{
                setSemester(e.target.value)
              }}
                type='text'
                placeholder='enter semester'
              />
            </Form.Group>
          </Col>
          <Col md={6} className='mb-3'>
            <Form.Group>
              <Form.Label>Sem code</Form.Label>
              <Form.Control
              value={semCode}
              onChange={(e)=>{
                setSemCode(e.target.value)
              }}
                type='text'
                placeholder='enter semester code'
              />
            </Form.Group>
          </Col>
        </Row>
        <Button>Submit</Button>
      </Form>
     </Card.Body>
    </Card>
  )
}

export default CreateSemester;