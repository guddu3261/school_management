import React from 'react';
import {Col,Row,Form,Button,Card} from '@themesberg/react-bootstrap'

const SallaryCreation = () => {
  return (
    <Card border='light' className='shadow-sm bg-white mb-0'>
      <Card.Body>
        <h5 className='text-center'>Salary Creation</h5>
          <Form>
            <Row>
                <Col className='mb-4' md={6} >
                    <Form.Group>
                        <Form.Label>Select Teacher </Form.Label>
                         <Form.Select>
                            <option>please select</option>
                             <option>Teacher 1</option>
                             <option>Teacher 2</option>
                             <option>Teacher 3</option>
                         </Form.Select>
                    </Form.Group>
                </Col>
            </Row>
            <h5 className='my-3'>Earning</h5>
            <Row className='mt-3'>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Basic pay</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='enter basic pay'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>House Rent Allowance</Form.Label>
                          <Form.Control
                            type='text'
                            placeholder='house rent'
                          />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Special Allowance</Form.Label>
                          <Form.Control
                            type='text'
                            placeholder='special Allowance'
                          />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Conveyance</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='conveyance'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Addnl special allowance</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='additional allowance'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Shift Allowance</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='shift allowance'
                        />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
            <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Medical Allowance</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='medical allowance'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Stat Bonus</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='stat allowance'
                        />
                    </Form.Group>
                </Col>
                <Col md={4} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Gross Earning</Form.Label>
                         <Form.Control
                            type='text'
                            placeholder='total earning'
                         />
                    </Form.Group>
                </Col>
            </Row>
            <h5 className='my-3'>Deductions</h5>
              <Row>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Provident fund</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='pf'
                        />
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Professional Tax</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder=' professional tax'
                        />
                    </Form.Group>
                </Col>
              </Row>
              <Row>
              <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Income fund</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='income tax'
                        />
                    </Form.Group>
                </Col>
                <Col md={6} className='mb-4'>
                    <Form.Group>
                        <Form.Label>Total Deduction</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='total deduction'
                        />
                    </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                    <Form.Group>
                        <Form.Label> Total no of Leaves</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='leave'
                        />
                    </Form.Group>
                </Col>
              </Row>
              <Button tye='submit' className='my-3' >create</Button>
          </Form>
      </Card.Body>
    </Card>
  )
}

export default SallaryCreation