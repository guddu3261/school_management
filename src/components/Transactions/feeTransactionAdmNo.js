import React from 'react';
import{Row,Col,Button,Form,Card,InputGroup,Table} from '@themesberg/react-bootstrap';

const feeTransactionAdmNo = () => {
  return (
    <Card border='light' className='bg-white shadow-sm mb-0'>
        <Card.Body>
            <Form>
                <h4 className='text-center' >Fee Transactions</h4>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select Payment Mode</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                                <option>Cash</option>
                                <option>Cheque</option>
                                <option>DD</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Bank Name:</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>DD/Cheque no:</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='dd/cheque no'
                            />
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Receipt Number:</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='receipt number'
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select Month</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select Fee Group</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Enter Admission Number</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter admission no'
                            />
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Student Name:</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='enter student name'
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select Class</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                    <Col md={6} className='mb-3'>
                        <Form.Group>
                            <Form.Label>Select Section</Form.Label>
                            <Form.Select>
                                <option>please select</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <Button type='submit' className="text-center" >submit</Button>
            </Form>
        </Card.Body>
        </Card>
  )
}

export default feeTransactionAdmNo;