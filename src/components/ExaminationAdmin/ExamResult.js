import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import './Class.css'
import {
  faCashRegister,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faPenSquare,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  InputGroup,
  Form,
} from "@themesberg/react-bootstrap";
// import Tooltip from '@mui/material/Tooltip'


const ExamResult = () => {  
    const[classArray,setClassArray]=useState([
        {class:"Class 1"},
        {class:"Class 2"},
        {class:"Class 3"},
        {class:"Class 4"},
        {class:"Class 5"},
        {class:"Class 6"},
        {class:"Class 7"},
        {class:"Class 8"},
        {class:"Class 9"},
        {class:"Class 10"},
    ])
  return (
   <Card className='bg-white shadow-sm mb-7' border="light">
    <Card.Body>
        <h4>Select Criteria</h4>
        
         <Row>
            <Col md={4} style={{marginLeft:"50rem"}} >
            <Form.Group id="topbarSearch">
                <InputGroup className="input-group-merge search-bar">
                  <InputGroup.Text><FontAwesomeIcon icon={faSearch} /></InputGroup.Text>
                  <Form.Control type="text" placeholder="Search" />
                </InputGroup>
              </Form.Group>
            </Col>
         </Row>
         
        <Row className='mt-4' >
            <Col md={3} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Exam</Form.Label>
               <Form.Select id='select_exam' defaultValue="0" >
               <option value="0">select Exam</option>
                  <option value="AL">Mid Term</option>
                  <option value="AK">Monthly Test</option>
                  <option value="AK">Practical exam</option>
                  <option value="AZ">Final Exam</option>
               </Form.Select>
            </Form.Group>
            </Col> 
            <Col md={3} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Session</Form.Label>
               <Form.Select id='select_session' defaultValue="0" >
               <option value="0">select Session</option>
                  <option value="AL">2018-2019</option>
                  <option value="AK">2019-2020</option>
                  <option value="AK">2020-2021</option>
                  <option value="AZ">2021-2022</option>
                  <option value="AZ">2022-2023</option>
               </Form.Select>
            </Form.Group>
            </Col> 
            <Col md={3} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Class</Form.Label>
               <Form.Select id='select_class' defaultValue="0" >
               <option value="0">select Class</option>
                    {
                        classArray.map((item,index)=>(
                           <option>{item.class} </option> 
                        ))
                    }
               </Form.Select>
            </Form.Group>
            </Col>
            <Col md={3} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Section</Form.Label>
               <Form.Select id='select_session' defaultValue="0" >
               <option value="0">select Section</option>
                  <option value="AL">A</option>
                  <option value="AK">B</option>
                  <option value="AK">C</option>
                  <option value="AZ">D</option>
               </Form.Select>
            </Form.Group>
            </Col> 
        </Row>
        <Button>Search</Button>
    </Card.Body>
   </Card>
  );
};

export default ExamResult;
