import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Datetime from 'react-datetime';
import moment from "moment-timezone";
// import './Class.css'
import {
  faCashRegister,
  faChartLine,
  faCloudUploadAlt,
  faPlus,
  faRocket,
  faTasks,
  faUserShield,
  faPenSquare,
  faCalendarAlt
} from "@fortawesome/free-solid-svg-icons";
import {
  Col,
  Row,
  Nav,
  Card,
  Image,
  Button,
  Table,
  Dropdown,
  ProgressBar,
  Pagination,
  ButtonGroup,
  Form,
  InputGroup
} from "@themesberg/react-bootstrap";
import grayback from '../../assets/img/icons/grayback.png'
// import Tooltip from '@mui/material/Tooltip'
const Attendance = () => {
  const [showForm, setShowForm] = useState(false);
  const [showTable, setShowTable] = useState(false);
  const [loaderFlag, setLoaderFlag] = useState(true);
  const handleShowAttendance = () => {
    // setShowForm(true);
    setShowTable(true);
  };
  const[attendanceDate,setAttendanceDate]=useState("")
const[classArray,setClassArray]=useState([
  {id:1,className:"LKG",section:"A"},
  {id:2,className:"UKG",section:"A"},
  {id:3,className:"1st Standard",section:"A"},
  {id:4,className:"2nd Standard",section:"A"},
  {id:5,className:"3rd Standard",section:"A"},
  {id:6,className:"4th Standard",section:"A"},
  {id:7,className:"5th Standard",section:"A"},
  {id:7,className:"6th Standard",section:"A"},
  {id:8,className:"7th Standard",section:"A"},
  {id:9,className:"8th Standard",section:"A"},
  {id:10,className:"9th Standard",section:"A"},
  {id:11,className:"10th Standard",section:"A"},
  
])
const[attendanceArray,setAttendanceArray]=useState([
  {id:111,name:"Rahul",regNo:"KA-123",rollNo:12,attendance:"present"},
  {id:211,name:"Kishan",regNo:"KA-135",rollNo:22,attendance:"Absent"},
  {id:311,name:"Kulka",regNo:"KA-127",rollNo:12,attendance:"present"},
  {id:411,name:"ramstu",regNo:"KA-123",rollNo:12,attendance:"present"},
  {id:511,name:"naresh",regNo:"KA-123",rollNo:12,attendance:"present"},
  {id:611,name:"kangana",regNo:"KA-123",rollNo:12,attendance:"Absent"},
  {id:711,name:"Ganesh",regNo:"KA-123",rollNo:12,attendance:"present"},
  {id:811,name:"Abc",regNo:"KA-123",rollNo:12,attendance:"Absent"},
  {id:911,name:"Xyz",regNo:"KA-123",rollNo:12,attendance:"present"},
])

  
  return (
    <>
  <Card className='bg-white shadow-sm mb-7' border="light">
    <Card.Body>
        <h4>Select Criteria</h4>
        <Row>
            <Col md={4} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Class</Form.Label>
               <Form.Select id='select_class' defaultValue="0" >
               <option value="0">select class</option>
               
                {

                  classArray.map((item)=>(
                    <option key={item.id}> {item.className} </option>
                  ))
                }
               
                 
               </Form.Select>
            </Form.Group>
            </Col> 
            <Col md={4} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Select Section</Form.Label>
               <Form.Select id='select_session' defaultValue="0" >
               <option value="0">select Section</option>
                  <option value="AL">A</option>
                  <option value="AK">B</option>
                  <option value="AK">C</option>
               </Form.Select>
            </Form.Group>
            </Col> 
            <Col md={4} className='mb-3'>
            <Form.Group className='mb-2' >
               <Form.Label>Attendance Date</Form.Label>
               <Datetime
                  timeFormat={false}
                  onChange={setAttendanceDate}
                  renderInput={(props, openCalendar) => (
                    <InputGroup>
                      <InputGroup.Text><FontAwesomeIcon icon={faCalendarAlt} /></InputGroup.Text>
                      <Form.Control
                        required
                        // value={values.dob}
                        // onChange={handleChange}
                        value={attendanceDate ? moment(attendanceDate).format("MM/DD/YYYY") : ""}
                        placeholder="mm/dd/yyyy"
                        onFocus={openCalendar}
                        onChange={() => { }} />
                    </InputGroup>
                  )} />
            </Form.Group>
            </Col>
           
        </Row>
        <Button  style={{float:"right"}} onClick={handleShowAttendance} >Search</Button>

        {showTable==true &&(
          <Card className="mt-7">
          <Card.Body>
            <h5>Student List</h5>
            <Table
              responsive
              className="table-centered table-nowrap rounded mb-0"
            >
              <thead className="thead-light">
                <tr>
                  <th className="border-0">#</th>
                  <th className="border-0">Roll No.</th>
                  <th className="border-0">Register No</th>
                  <th className="border-0">Student Name</th>
                  <th className="border-0">Attendance</th>
                  <th className="border-0">Note</th>
             
                </tr>
              </thead>
              <tbody>
                {attendanceArray.map((item,i) => (
                  <tr key ={i}>
                    <td>{i+1}</td>
                    <td>{item.rollNo}</td>
                    <td>{item.regNo}</td>
                    <td>{item.name}</td>
                    <td>{item.attendance}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
        )}
    </Card.Body>
   </Card>
      
    </>
  );
};

export default Attendance;
