
export const Routes = {
    // pages
    Signin: { path: "/" },
    HomePage:{path:'/homepage'},
    Presentation: { path: "/teacherregistration" },
    DashboardOverview: { path: "/dashboard/overview" },
    Class:{path:"/Class-table"},
    Teacher:{path:"/teachers"},
    Class_Section:{path:"/class_section"},
    Attendance:{path:"/Attendance"},
    ExamTypes:{path:"/exam_time_types"},
    HallTicketGenaration:{path:"/hall_ticket"},
    ExamResult:{path:"/exam_result"},
    AttendanceDisplay:{path:"/attendance_display"},
    AttendanceReport:{path:"/attendance_report"},
    StudentAttendanceEntry:{path:"/attendance_entry"},
    StudentAttendanceUpdate:{path:"/attendance_update"},
    TeacherAttendanceReport:{path:"/teacher_attendance_report"},
    TeacherAttendanceEntry:{path:"/teacher_attendance_entry"},
    LeaveTypes:{path:"/leave_types"},
    LeaveDetails:{path:"/leave_details"},
    LeaveApprove:{path:"/leave_approve"},
    LeaveApply:{path:"/leave_apply"},
    FeeGroup:{path:"/fee_group"},
    FeeHead:{path:"/fee_head"},
    StudentFeeMapping:{path:"/student_fee_mapping"},
    feeTransactions:{path:"/fee_transactions"},
    AddAdmin:{path:"/add_admin"},
    AddDay:{path:"/add_day"},
    AddPeriod:{path:"/add_period"},
    AddPreviousDate:{path:"/add_previousdate"},
    AddHoliday:{path:"/add_holiday"},
    AddImages:{path:"/add_images"},
    AddVideos:{path:"/AddVideos"},
    AddVechiles:{path:"/Add_vechiles"},
    AddDrivers:{path:"/add_drivers"},
    setRoutes:{path:"/set_routes"},
    PayRollStandard:{path:"/payroll_standard"},
    HolidayCalender:{path:"/holiday_calender"},
    CreateSchool:{path:"/create_school"},
    CreateCollege:{path:"/create_college"},
    CreateUniversity:{path:"/create_university"},
    Semester:{path:"/add_semester"},
    SallaryCreation:{path:"/sallary_creation"},
    SallaryReport:{path:"/sallary_report"},
    AddReceipt:{path:"/add_receipt"},
    StudentList:{path:"/student_list"},
    add_department:{path:"/add_department"},
    add_subject:{path:"/add_subject"},
    StudentPasswordSetting:{path:"/student_password"},
    TeacherPasswordSetting:{path:"/teacher_password"},
    Transactions: { path: "/transactions" },
    feeTransactionAdmNo: { path: "/transaction_adm_no" },
    Settings: { path: "/settings" },
    Teacher_reg:{path:"/teacherregistration"},
    create_subject:{path:'/create_subject'},
    Upgrade: { path: "/upgrade" },
    BootstrapTables: { path: "/tables/bootstrap-tables" },
    Billing: { path: "/examples/billing" },
    Invoice: { path: "/examples/invoice" },
  //  Signin: { path: "/examples/sign-in" },
    Signup: { path: "/examples/sign-up" },
    ForgotPassword: { path: "/examples/forgot-password" },
    ResetPassword: { path: "/examples/reset-password" },
    Lock: { path: "/examples/lock" },
    NotFound: { path: "/examples/404" },
    ServerError: { path: "/examples/500" },

    // docs
    DocsOverview: { path: "/documentation/overview" },
    DocsDownload: { path: "/documentation/download" },
    DocsQuickStart: { path: "/documentation/quick-start" },
    DocsLicense: { path: "/documentation/license" },
    DocsFolderStructure: { path: "/documentation/folder-structure" },
    DocsBuild: { path: "/documentation/build-tools" },
    DocsChangelog: { path: "/documentation/changelog" },

    // components
    Accordions: { path: "/components/accordions" },
    Alerts: { path: "/components/alerts" },
    Badges: { path: "/components/badges" },
    Widgets: { path: "/widgets" },
    Breadcrumbs: { path: "/components/breadcrumbs" },
    Buttons: { path: "/components/buttons" },
    Forms: { path: "/components/forms" },
    Modals: { path: "/components/modals" },
    Navs: { path: "/components/navs" },
    Navbars: { path: "/components/navbars" },
    Pagination: { path: "/components/pagination" },
    Popovers: { path: "/components/popovers" },
    Progress: { path: "/components/progress" },
    Tables: { path: "/components/tables" },
    Tabs: { path: "/components/tabs" },
    Tooltips: { path: "/components/tooltips" },
    Toasts: { path: "/components/toasts" },
    WidgetsComponent: { path: "/components/widgets" }
};