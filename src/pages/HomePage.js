import React, { useState, useEffect } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import { Routes } from "../routes";
// pages
import Presentation from "./Presentation";
import Upgrade from "./Upgrade";
import DashboardOverview from "./dashboard/DashboardOverview";
import Transactions from "./Transactions";
import Settings from "./Settings";
import BootstrapTables from "./tables/BootstrapTables";
import Signin from "./examples/Signin";
import Signup from "./examples/Signup";
import ForgotPassword from "./examples/ForgotPassword";
import ResetPassword from "./examples/ResetPassword";
import Lock from "./examples/Lock";
import NotFoundPage from "./examples/NotFound";
import ServerError from "./examples/ServerError";
import Attendance from '../components/Student/Attendance';
// documentation pages
import DocsOverview from "./documentation/DocsOverview";
import DocsDownload from "./documentation/DocsDownload";
import DocsQuickStart from "./documentation/DocsQuickStart";
import DocsLicense from "./documentation/DocsLicense";
import DocsFolderStructure from "./documentation/DocsFolderStructure";
import DocsBuild from "./documentation/DocsBuild";
import DocsChangelog from "./documentation/DocsChangelog";
// components
import Sidebar from "../components/Sidebar";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Preloader from "../components/Preloader";

import Accordion from "./components/Accordion";
import Alerts from "./components/Alerts";
import Badges from "./components/Badges";
import Breadcrumbs from "./components/Breadcrumbs";
import Buttons from "./components/Buttons";
import Forms from "./components/Forms";
import Modals from "./components/Modals";
import Navs from "./components/Navs";
import Navbars from "./components/Navbars";
import Pagination from "./components/Pagination";
import Popovers from "./components/Popovers";
import Progress from "./components/Progress";
import Tables from "./components/Tables";
import Tabs from "./components/Tabs";
import Tooltips from "./components/Tooltips";
import Toasts from "./components/Toasts";
import Teacher_reg from './Teacher_reg';
import Class from '../components/Class/Class';
import Class_section from '../components/Class/Class_section';
import Teachers from '../components/Teacher/Teachers';
import ExamTypes from '../components/ExaminationAdmin/ExamTypes';
import HallTicketGenaration from '../components/ExaminationAdmin/HallTicketGenaration';
import ExamResult from '../components/ExaminationAdmin/ExamResult';
import HolidayCalender from '../components/HolidayClaenderAdmin/HolidayCalender';
import CreateSchool from "../components/CreateSchool";
import CreateCollege from "../components/CreateColege";
import CreateUniversity from "../components/CreateUniversity";
import CreateSemester from "../components/CreateSemester";
import AddReceipt from '../components/AddReceipt';
import StudentList from '../components/StudentList';
import StudentPasswordSetting from "../components/StudentPasswordSetting";
import TeacherPasswordSetting from '../components/TeacherPasswordSetting';
import AttendanceDisplay from '../components/StudentAttendance/AttendanceDisplay'
import AttendanceReport from '../components/StudentAttendance/AttendanceReport';
import StudentAttendanceEntry from '../components/StudentAttendance/StudentAttendanceEntry';
import StudentAttendanceUpdate from '../components/StudentAttendance/StudentAttendanceUpdate';
import TeacherAttendanceReport from '../components/TeacherAttendance/TeacherAttendanceReport';
import TeacherAttendanceEntry from '../components/TeacherAttendance/TeacherAttendanceEntry';
import LeaveTypes from '../components/LeaveManagement/LeaveTypes';
import LeaveDetails from '../components/LeaveManagement/LeaveDetails';
import LeaveApprove from '../components/LeaveManagement/LeaveApprove';
import LeaveApply from '../components/LeaveManagement/LeaveApply';
import AddHoliday from "../components/HolidayClaenderAdmin/AddHoliday";
import AddPreviousDate from "../components/HolidayClaenderAdmin/AddPreviousDate";
import FeeGroup from '../components/Configurationss/FeeGroup';
import FeeHead from '../components/Configurationss/FeeHead';
import StudentFeeMapping from '../components/Configurationss/StudentFeeMapping';
import FeeTransactions from '../components/Transactions/FeeTransactions';
import AddDay from '../components/TimeTable/AddDay';
import AddPeriod from '../components/TimeTable/AddPeriod';
import AddImages from '../components/Gallery/AddImages';
import AddVideos from '../components/Gallery/AddVideos';
import feeTransactionAdmNo from '../components/Transactions/feeTransactionAdmNo';
import AddVechiles from '../components/Transportation/AddVechiles';
import AddDrivers from '../components/Transportation/AddDrivers';
import setRoutes from '../components/Transportation/setRoutes';
import PayRollStandard from '../components/Payroll/PayRollStandard';
import AddAdmin from '../components/AddAdmin/AddAdmin';
import SallaryCreation from '../components/SallaryCreation/SallaryCreation';
import SallaryReport from '../components/SallaryReport/SallaryReport';
// import CreateSubject from '../components/CreateSubject/CreateSubject';
import AddDepartment from '../components/AddDepartment/AddDepartment';
import AddSubject from '../components/AddSubject/AddSubject';
import CreateSubject from '../components/CreateSubject/CreateSubject';


const RouteWithLoader = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Route {...rest} render={props => ( <> <Preloader show={loaded ? false : true} /> <Component {...props} /> </> ) } />
  );
};

const RouteWithSidebar = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000);
    return () => clearTimeout(timer);
  }, []);

  const localStorageIsSettingsVisible = () => {
    return localStorage.getItem('settingsVisible') === 'false' ? false : true
  }

  const [showSettings, setShowSettings] = useState(localStorageIsSettingsVisible);

  const toggleSettings = () => {
    setShowSettings(!showSettings);
    localStorage.setItem('settingsVisible', !showSettings);
  }

  const [sidebarOpen, setSideBarOpen] = useState(false);
  const handleViewSidebar = () => {
    setSideBarOpen(!sidebarOpen);
  };

  return (
    <Route {...rest} render={props => (
      <>
        <Preloader show={loaded ? false : true} />
        <Sidebar isOpen={sidebarOpen} toggleSidebar={handleViewSidebar} />

        <main className="content">
          <Navbar />
          <Component {...props} />
          <Footer toggleSettings={toggleSettings} showSettings={showSettings} />
        </main>
      </>
    )}
    />
  );
};

export default () => (
  <Switch>
    {/* <RouteWithLoader exact path={Routes.Presentation.path} component={Presentation} /> */}
    <RouteWithLoader exact path={Routes.Signin.path} component={Signin} />
    <RouteWithLoader exact path={Routes.Signup.path} component={Signup} />
    <RouteWithLoader exact path={Routes.ForgotPassword.path} component={ForgotPassword} />
    <RouteWithLoader exact path={Routes.ResetPassword.path} component={ResetPassword} />
    <RouteWithLoader exact path={Routes.Lock.path} component={Lock} />
    <RouteWithLoader exact path={Routes.NotFound.path} component={NotFoundPage} />
    <RouteWithLoader exact path={Routes.ServerError.path} component={ServerError} />

    {/* pages */}
    <RouteWithSidebar exact path={Routes.DashboardOverview.path} component={DashboardOverview} />
    <RouteWithSidebar exact path={Routes.Upgrade.path} component={Upgrade} />
    <RouteWithSidebar exact path={Routes.Transactions.path} component={Transactions} />
    <RouteWithSidebar exact path={Routes.Settings.path} component={Settings} />
    <RouteWithSidebar exact path={Routes.StudentList.path} component={StudentList} />
    <RouteWithSidebar exact path={Routes.Settings.path} component={Settings} />
    <RouteWithSidebar exact path={Routes.Teacher_reg.path} component={Teacher_reg}/>
    <RouteWithSidebar exact path={Routes.Class.path} component={Class}/>
    <RouteWithSidebar exact path={Routes.Teacher.path} component={Teachers}/>
    <RouteWithSidebar exact path={Routes.Class_Section.path} component={Class_section}/>
    <RouteWithSidebar exact path={Routes.Attendance.path} component={Attendance}/>
    <RouteWithSidebar exact path={Routes.BootstrapTables.path} component={BootstrapTables} />
    <RouteWithSidebar exact path={Routes.ExamTypes.path} component={ExamTypes} />
    <RouteWithSidebar exact path={Routes.HallTicketGenaration.path} component={HallTicketGenaration} />
    <RouteWithSidebar exact path={Routes.ExamResult.path} component={ExamResult} />
    <RouteWithSidebar exact path={Routes.HolidayCalender.path} component={HolidayCalender} />
    <RouteWithSidebar exact path={Routes.CreateSchool.path} component={CreateSchool} />
    <RouteWithSidebar exact path={Routes.CreateCollege.path} component={CreateCollege} />
    <RouteWithSidebar exact path={Routes.CreateUniversity.path} component={CreateUniversity} />
    <RouteWithSidebar exact path={Routes.Semester.path} component={CreateSemester} />
    <RouteWithSidebar exact path={Routes.AddReceipt.path} component={AddReceipt} />
    <RouteWithSidebar exact path={Routes.AddHoliday.path} component={AddHoliday} />
    <RouteWithSidebar exact path={Routes.AddPreviousDate.path} component={AddPreviousDate} />
    <RouteWithSidebar exact path={Routes.StudentPasswordSetting.path} component={StudentPasswordSetting} />
    <RouteWithSidebar exact path={Routes.TeacherPasswordSetting.path} component={TeacherPasswordSetting} />
    <RouteWithSidebar exact path={Routes.AttendanceDisplay.path} component={AttendanceDisplay} />
    <RouteWithSidebar exact path={Routes.AttendanceReport.path} component={AttendanceReport} />
    <RouteWithSidebar exact path={Routes.StudentAttendanceEntry.path} component={StudentAttendanceEntry} />
    <RouteWithSidebar exact path={Routes.StudentAttendanceUpdate.path} component={StudentAttendanceUpdate} />
    <RouteWithSidebar exact path={Routes.TeacherAttendanceReport.path} component={TeacherAttendanceReport} />
    <RouteWithSidebar exact path={Routes.TeacherAttendanceEntry.path} component={TeacherAttendanceEntry} />
    <RouteWithSidebar exact path={Routes.LeaveTypes.path} component={LeaveTypes} />
    <RouteWithSidebar exact path={Routes.LeaveDetails.path} component={LeaveDetails} />
    <RouteWithSidebar exact path={Routes.LeaveApprove.path} component={LeaveApprove} />
    <RouteWithSidebar exact path={Routes.LeaveApply.path} component={LeaveApply} />
    <RouteWithSidebar exact path={Routes.FeeGroup.path} component={FeeGroup} />
    <RouteWithSidebar exact path={Routes.FeeHead.path} component={FeeHead} />
    <RouteWithSidebar exact path={Routes.StudentFeeMapping.path} component={StudentFeeMapping} />
    <RouteWithSidebar exact path={Routes.feeTransactions.path} component={FeeTransactions} />
    <RouteWithSidebar exact path={Routes.AddPeriod.path} component={AddPeriod} />
    <RouteWithSidebar exact path={Routes.AddDay.path} component={AddDay} />
    <RouteWithSidebar exact path={Routes.AddImages.path} component={AddImages} />
    <RouteWithSidebar exact path={Routes.AddVideos.path} component={AddVideos} />
    <RouteWithSidebar exact path={Routes.feeTransactionAdmNo.path} component={feeTransactionAdmNo} />
    <RouteWithSidebar exact path={Routes.AddVechiles.path} component={AddVechiles} />
    <RouteWithSidebar exact path={Routes.AddDrivers.path} component={AddDrivers} />
    <RouteWithSidebar exact path={Routes.setRoutes.path} component={setRoutes} />
    <RouteWithSidebar exact path={Routes.PayRollStandard.path} component={PayRollStandard} />
    <RouteWithSidebar exact path={Routes.AddAdmin.path} component={AddAdmin}/>
    <RouteWithSidebar exact path={Routes.SallaryCreation.path} component={SallaryCreation}/>
    <RouteWithSidebar exact path={Routes.SallaryReport.path} component={SallaryReport}/>
    <RouteWithSidebar exact path={Routes.add_department.path} component={AddDepartment}/>
    <RouteWithSidebar exact path={Routes.add_subject.path} component={AddSubject}/>
    <RouteWithSidebar exact path={Routes.create_subject.path} component={CreateSubject}/>
    {/* components */}
    <RouteWithSidebar exact path={Routes.Accordions.path} component={Accordion} />
    <RouteWithSidebar exact path={Routes.Alerts.path} component={Alerts} />
    <RouteWithSidebar exact path={Routes.Badges.path} component={Badges} />
    <RouteWithSidebar exact path={Routes.Breadcrumbs.path} component={Breadcrumbs} />
    <RouteWithSidebar exact path={Routes.Buttons.path} component={Buttons} />
    <RouteWithSidebar exact path={Routes.Forms.path} component={Forms} />
    <RouteWithSidebar exact path={Routes.Modals.path} component={Modals} />
    <RouteWithSidebar exact path={Routes.Navs.path} component={Navs} />
    <RouteWithSidebar exact path={Routes.Navbars.path} component={Navbars} />
    <RouteWithSidebar exact path={Routes.Pagination.path} component={Pagination} />
    <RouteWithSidebar exact path={Routes.Popovers.path} component={Popovers} />
    <RouteWithSidebar exact path={Routes.Progress.path} component={Progress} />
    <RouteWithSidebar exact path={Routes.Tables.path} component={Tables} />
    <RouteWithSidebar exact path={Routes.Tabs.path} component={Tabs} />
    <RouteWithSidebar exact path={Routes.Tooltips.path} component={Tooltips} />
    <RouteWithSidebar exact path={Routes.Toasts.path} component={Toasts} />
    {/* documentation */}
    <RouteWithSidebar exact path={Routes.DocsOverview.path} component={DocsOverview} />
    <RouteWithSidebar exact path={Routes.DocsDownload.path} component={DocsDownload} />
    <RouteWithSidebar exact path={Routes.DocsQuickStart.path} component={DocsQuickStart} />
    <RouteWithSidebar exact path={Routes.DocsLicense.path} component={DocsLicense} />
    <RouteWithSidebar exact path={Routes.DocsFolderStructure.path} component={DocsFolderStructure} />
    <RouteWithSidebar exact path={Routes.DocsBuild.path} component={DocsBuild} />
    <RouteWithSidebar exact path={Routes.DocsChangelog.path} component={DocsChangelog} />
    

    <Redirect to={Routes.NotFound.path} />
  </Switch>
);
