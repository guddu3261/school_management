import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faEnvelope, faUnlockAlt } from "@fortawesome/free-solid-svg-icons";
import { faFacebookF, faGithub, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { Col, Row, Form, Card, Button, FormCheck, Container, InputGroup } from '@themesberg/react-bootstrap';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { Routes } from "../../routes";
import BgImage from "../../assets/img/illustrations/signin.svg";
import { Formik } from "formik";
import * as Yup from 'yup';
import axios from 'axios'
export default () => {
  let history = useHistory();
  
   function handleSubmitForm(values) {
    debugger;
    console.log(values);
    history.push("/dashboard/overview");
    // event.preventDefault();
    // await submitForm(event.target);
    
  }

// Login Api Integration
// const handleSubmitForm=async(values,{resetForm})=>{
//   debugger;
//   resetForm({
//     email:"",
//     password:"",
//   })
//   const url="http://192.168.0.115/kkrtc-bus/server.php/api/UserSignin";
//   const data1={
//       username:values.email,
//       password:values.password
//   }
//   console.log(data1);
//   await axios.post(url,data1).then((response)=>{
//     console.log(response);
//     if(response.data.status == true){
//         // do stuffs
//         history.push("/dashboard/overview");
//         console.log(response.data);
//     }
//     else{
//       alert(response.data.message)
//     }
//   }).catch((error)=>{
//     console.log("Loogin Errror",error);
//     // console.log(error.response.data.message);
//   })
// }


// const handleSubmitForm=async(values)=>{
//   const url="https://fakestoreapi.com/auth/login";
//   const data={
//     username: values.email,
//     password:values.password,
//   }
// await axios.post(url,data).then((response)=>{
// if(response.data.status){
//   history.push("/dashboard/overview")
// }
// else{
//   window.alert('please enter valid emailid and password')
// }
// }).catch((error)=>{
//   console.log("signin Error", error)
// })

// }





 const initialValue={
  email:"",
  password:"",
 }
const validationSchema=Yup.object().shape({
  // email:Yup.string().email('enter a valid email').required("must be enter email"),
  password:Yup.string().required('must be enter password'),
})
//  const handleLogin=()=>{
//   // navigate(path)
//   console.log('SuccessFully Login');
//   navigate('../HomePage.js')
//  }
  return (
    <main>
      <section className="d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
        <Container>
          <p className="text-center">
            <Card.Link as={Link} to={Routes.DashboardOverview.path} className="text-gray-700">
              <FontAwesomeIcon icon={faAngleLeft} className="me-2" /> Back to homepage
            </Card.Link>
          </p>
          <Row className="justify-content-center form-bg-image" style={{ backgroundImage: `url(${BgImage})` }}>
            <Col xs={12} className="d-flex align-items-center justify-content-center">
              <div className="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                <div className="text-center text-md-center mb-4 mt-md-0">
                  <h3 className="mb-0">Sign in to our platform</h3>
                </div>
               <Formik
               initialValues={initialValue}
               validationSchema={validationSchema}
               onSubmit={handleSubmitForm}
               >
                {({
                    errors,
                    values,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    resetForm,
                    isSubmitting,
                    setFieldValue,
                    setFieldTouched
                })=>{
                  return(
                    <Form className="mt-4" onSubmit={handleSubmit} >
                  <Form.Group id="email" className="mb-4">
                    <Form.Label>Your Email</Form.Label>
                    <InputGroup>
                      <InputGroup.Text>
                        <FontAwesomeIcon icon={faEnvelope} />
                      </InputGroup.Text>
                      <Form.Control 
                      autoFocus
                      type="text" 
                      name='email' 
                      placeholder="example@company.com"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                      errors={errors.email}
                       />
                    </InputGroup>
                    <div style={{color:"red"}} >
                  {errors.email && touched.email && errors.email}
                 </div>
                  </Form.Group>
                  <Form.Group>
                    <Form.Group id="password" className="mb-4">
                      <Form.Label>Your Password</Form.Label>
                      <InputGroup>
                        <InputGroup.Text>
                          <FontAwesomeIcon icon={faUnlockAlt} />
                        </InputGroup.Text>
                        <Form.Control
                          type="password"
                          name='password'
                           placeholder="Password"
                           onChange={handleChange}
                            onBlur={handleBlur}
                           value={values.password}
                            error={errors.password}
                            />
                      </InputGroup>
                      <div style={{color:"red"}} >
                  {errors.password && touched.password && errors.password}
                 </div>
                    </Form.Group>
                    <div className="d-flex justify-content-between align-items-center mb-4">
                      <Form.Check type="checkbox">
                        <FormCheck.Input id="defaultCheck5" className="me-2" />
                        <FormCheck.Label htmlFor="defaultCheck5" className="mb-0">Remember me</FormCheck.Label>
                      </Form.Check>
                      <Card.Link className="small text-end">Forgot password?</Card.Link>
                    </div>
                  </Form.Group>
                  <Button variant="primary" type="submit" className="w-100">
                    Sign in
                  </Button>
                </Form>
                  )
                }}
               </Formik>

                <div className="mt-3 mb-4 text-center">
                  <span className="fw-normal">or login with</span>
                </div>
                <div className="d-flex justify-content-center my-4">
                  <Button variant="outline-light" className="btn-icon-only btn-pill text-facebook me-2">
                    <FontAwesomeIcon icon={faFacebookF} />
                  </Button>
                  <Button variant="outline-light" className="btn-icon-only btn-pill text-twitter me-2">
                    <FontAwesomeIcon icon={faTwitter} />
                  </Button>
                  <Button variant="outline-light" className="btn-icon-only btn-pil text-dark">
                    <FontAwesomeIcon icon={faGithub} />
                  </Button>
                </div>
                <div className="d-flex justify-content-center align-items-center mt-4">
                  <span className="fw-normal">
                    Not registered?
                    <Card.Link as={Link} to={Routes.Signup.path} className="fw-bold">
                      {` Create account `}
                    </Card.Link>
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </main>
  );
};
