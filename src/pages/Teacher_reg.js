import React from 'react';
import { Col, Row, Button, Dropdown } from '@themesberg/react-bootstrap';
import TeacherRegistration from '../components/TeacherRegistration';

const Teacher_reg = () => {
  return (
    <>
        <Row>
        <Col xs={12} xl={12}>
          <TeacherRegistration />
        </Col>
        </Row>
    </>
  )
}

export default Teacher_reg